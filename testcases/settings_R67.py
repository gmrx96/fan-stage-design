import numpy as np


#---------------------------------------------------------------------------------------------#
#------------------------------REQUESTED VARIABLES FOR SIMULATION-----------------------------#
#---------------------------------------------------------------------------------------------#

GEOMETRY = "EXISTING"            # Problem to study :: DESIGN for meangen to create parablade file,
                                 # :: EXISTING for parablade existing file (from matchblade)

MACHINE_NAME = "Rotor67"         # Existing machine name

N_STAGE = 1                      # Number of stages in the machine(integer)
N_ROWS = 1                       # Number of rows in each stage (integer)
dim = 3                          # Dimension of the final blade for parablade
TYPE = "C"                       # Axial machine type :: C for compressor :: T for turbine

Pt_in = 101325                    # Inlet total pressure [Pa]
Tt_in = 288.2                     # Inlet total temperature [k]
P_out = 126644                  # Outlet static P  
RPM = -16043                    # RPM

#---------------------------Gas properties - air--------------------------------#

R = 287.06                       # Air specific gas constant [J/(Kg*K)]
gamma = 1.4
Cp = 1006                        # Specific heat [J/Kg*K]
Tcrit = 273.15                   # Critical temperature [K]
Pcrit = 101325                   # Critical pressure [N/m^2]


# ---------------------------- Freestream variables------------------------------#
# You can get these from the preliminary CFX run of the machine (PHISICAL BLADE SYMULATION) if not given
T_in = 271.516                   # Inlet static temperature [k]
P_in = 82242.7                   # Inlet static pressure [Pa]
rho_in = 1.05503                  # Inlet density [kg/m^3] 
M_in = 0.554153                   # Inlet Mach 
V_in = 183.083                  # Inlet axial velocity [m/s]





#---------------------------------------------------------------------------------------------#
#----------------------------------------OPTIONS----------------------------------------------#
#---------------------------------------------------------------------------------------------#

# Blade plot option for PlotBlade :: YES :: NO
PLOT_BLADE = "NO"

# BFM mesh option :: YES :: NO
MESH_BFM = "NO"

# BLADE mesh option :: YES :: NO
MESH_BLADE = "NO"

BFM_MODEL = "THOLLET"

# SENSITIVITY analysis option :: YES :: NO
SENSITIVITY = "YES"

# OPTIZATION analysis option :: YES :: NO
OPTIMIZATION = "NO"

# PLOT comparison between BFM and CFX :: YES :: NO
PLOT_COMPARISON = "NO"

ADJOINT = "NO"

SOLVE = "MANUAL"

BFM_POSTPROCESS = "NO"

CFX_POSTPROCESS = "NO"



# ---------------------------------------------------------------------------- #
# ----------------------Mesh parameters------------------------------- #
# ---------------------------------------------------------------------------- #

# Wedge angle of 3D domain in degrees. Should be lower than 180 degrees.
WEDGE = 3.0

# Node count in axial direction within the blade zones. Should be an integer.
AXIAL_POINTS = 100

RADIAL_POINTS = 40


TANGENTIAL_POINTS = 3

# Number of boundary layers in the blade mesh.
BOUNDARY_LAYER_COUNT = 5

# Thickness of first boundary layer in blade mesh.
BOUNDARY_LAYER_THICKNESS = 1.5e-5
# BOUNDARY_LAYER_THICKNESS = 2e-6

# Y+ desired
Y_PLUS = 1

# Number of sections per wedge degree. Should be an integer.
SECTIONS_PER_DEGREE = 1

# Cell size multiplication factor at the inlet.
LE_FACTOR = 1.2

# Cell size multiplication factor at the outlet.
TE_FACTOR = 1.2


# Cell size multiplication factor at the inlet.
INLET_FACTOR = 1.2

# Cell size multiplication factor at the outlet.
OUTLET_FACTOR = 1.2

# Cell size multiplication factor at the blade surface.
BLADE_FACTOR = 0.3

# Mesh plot option. If 'YES', it will use the gmesh plot option to visualize the 3D mesh. If 'NO', it will skip this.
PLOT_MESH = "NO"





