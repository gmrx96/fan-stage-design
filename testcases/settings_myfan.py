import numpy as np



# Initialize design variables
GEOMETRY = "DESIGN"
MACHINE_NAME = "Myfan"


N_STAGE = 1
N_ROWS = 1                       # Number of rows in each stage (integer)
dim = 3                          # Dimension of the final blade for parablade
TYPE = 'C'
  
# # CFM56-5C--------------------------------------  
# #   rho 0.28852
# #   tt 243.8745
# #   Pt 27153.4059
# P_in = 17943
# T_in = 216.65
# m_dot = 100                      # mass flow rate [kg/s]
# beta_TT = 1.4                    # compression ratio
# # M_in = 0.4                       # Mach number of the vehicle


# Generic subsonic fan
# P_in = 101325                      # Inlet pressure [Pa]
# T_in = 288.15                      # Inlet temperature [k]
Pt_in = 101325                      # Inlet pressure [Pa]
Tt_in = 288.15                      # Inlet temperature [k]
Pt_in = 120000                     # Inlet pressure [Pa]
Tt_in = 303                      # Inlet temperature [k]
m_dot = 60                       # mass flow rate [kg/s]
beta_TT = 1.3                      # compression ratio

ki = 0.8                           # Reaction rate
phi = 0.6                       # Flow coefficient
psi = 0.3                      # Work coefficient

# M_in = 0.5                         # Mach number of the vehicle



# # Subsonic fan coefficient definition
# Pt_in = 17943                      # Inlet total pressure [Pa]
# Tt_in = 216.65                      # Inlet total temperature [k]
# m_dot = 10                       # mass flow rate [kg/s]
# beta_TT = 1.3                      # compression ratio
# ki = 1                           # Reaction rate
# phi = 0.4                        # Flow coefficient
# psi = 0.4                         # Work coefficient

#---------------------------------Geometry------------------------------------#
Rm = 0.25                     # mean radius [m]
c = 0.05                         # blade chord [m]
sigma = 1                        # solidity
s=c/sigma                        # Blade pitch  [m]
tip_cl = 0                   # Tip clearance [m]
t_max = 0.1                      # Max_tickness/chord  %
# shape = "double circular arc"    # Blade profile shape
shape = "double circular arc"    # Blade profile shape
r_gap = 0.5                      # Row gap with respect to the chord
s_gap = 1                        # Stage gap with respect to the chord 
twist = 0                        # Blade twist between 0 (straight) and 1 (free vortex)
QO_LE_R = 90
QO_TE_R = 90
QO_LE_S = 90
QO_TE_S = 90
N = round(sigma *2*np.pi*Rm/c)      # Blade number
# sigma = N/2/np.pi/Rm*c


#---------------------------Gas properties - air--------------------------------#

R = 287.06                       # Air specific gas constant [J/(Kg*K)]
gamma = 1.4
Cp = 1006                        # Specific heat [J/Kg*K]
Tcrit = 273.15                   # Critical temperature [K]
Pcrit = 101325                   # Critical pressure [N/m^2]

#----------------------- Boundary layer shape factor-----------------------------#
H = 1.08                         # Usual approximation

#--------------------Assumptions on stage velocity triangles---------------------#
alpha1 = 0                       # Axial inlet
alpha3 = 0                       # Axial outlet since a single fan stage

# Compute initial duty coefficients guess from Vavra
    # Assumptions:  - Va = constant 
    #               - rho = constant   
    #               - b = constant     (blade height)


eta_TT_stage = 0.90                      # Stage total-to-total efficiency, this is an initial guess from the selected parameters
eta_TT_r = eta_TT_stage                     # Initial guess for the rotor total-to-total efficiency is taken equal to the stage one
beta_r = beta_TT                   # Rotor static compression ratio, used only in the mono-rotor configuration (N_ROWS = 1)
# RPM = 7000                      # From CFM56-5C
# U = RPM*0.10472*Rm


# Compute inlet total P and T
# V_in = phi * U 
# rho_in = P_in/(T_in*R) 
# M_in = V_in / np.sqrt(gamma*R*T_in)
# # V_in = M_in* np.sqrt(gamma*R*T_in)
# Tt_in = T_in + 0.5*(V_in**2)/Cp
# Pt_in =P_in + 0.5*rho_in*V_in**2







#----------------------------------------OPTIONS----------------------------------------------#

# Blade plot option for PlotBlade :: YES :: NO
PLOT_BLADE = "NO"

# BFM mesh option :: YES :: NO
MESH_BFM = "YES"

BFM_MODEL = "THOLLET"

# BLADE mesh option :: YES :: NO
MESH_BLADE = "NO"

# SENSITIVITY analysis option :: YES :: NO
SENSITIVITY = "NO"

# OPTIZATION analysis option :: YES :: NO
OPTIMIZATION = "NO"

# PLOT comparison between BFM and CFX :: YES :: NO
PLOT_COMPARISON = "NO"

ADJOINT = "NO"

SOLVE = "MANUAL"

BFM_POSTPROCESS = "YES"

CFX_POSTPROCESS = "NO"

# ---------------------------------------------------------------------------- #
# ----------------------Mesh parameters------------------------------- #
# ---------------------------------------------------------------------------- #

# Wedge angle of 3D domain in degrees. Should be lower than 180 degrees.
WEDGE = 1.0

# Node count in axial direction within the blade zones. Should be an integer.
AXIAL_POINTS = 70

RADIAL_POINTS = 40

TANGENTIAL_POINTS = 3

# Number of boundary layers in the blade mesh.
BOUNDARY_LAYER_COUNT = 5

# Thickness of first boundary layer in blade mesh.
BOUNDARY_LAYER_THICKNESS = 1.4e-5

# Y+ desired
Y_PLUS = 1

# Number of sections per wedge degree. Should be an integer.
SECTIONS_PER_DEGREE = 1

# Cell size multiplication factor at the inlet.
LE_FACTOR = 1.2

# Cell size multiplication factor at the outlet.
TE_FACTOR = 1.2

# Cell size multiplication factor at the inlet.
INLET_FACTOR = 1.2

# Cell size multiplication factor at the outlet.
OUTLET_FACTOR = 1.2

# Cell size multiplication factor at the blade surface.
BLADE_FACTOR = 0.3

# Mesh plot option. If 'YES', it will use the gmesh plot option to visualize the 3D mesh. If 'NO', it will skip this.
PLOT_MESH = "NO"

ADJOINT = "NO"

SOLVE = "MANUAL"






