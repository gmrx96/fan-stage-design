import os
import shutil
import time
import sys
from DESIGN_GEOMETRY import DESIGN_GEOMETRY
from EXISTING_GEOMETRY import EXISTING_GEOMETRY
from SETTINGS_CLASS import *
import numpy as np
import math
from subprocess import Popen, PIPE

from FILE_CREATION import *



# Reading input file
HOME = os.getcwd()
TESTCASES = HOME + "\\testcases\\"


# ------------------manual input-----------------------------------------
INFile = TESTCASES + "settings_myfan.py"
# INFile = TESTCASES  + "settings_R67.py"
#-------------------------------------------------------------------------



# try:
#     INFile = DIR + sys.argv[-1]
# except:
#     raise Exception('\n\n\n''Something went wrong when reading the configuration file,exiting the program...'
#                     '\n\nTo call MakeBlade.py from terminal type:'
#                     '\n\tMakeBlade.py <configuration file name>')
t_start = time.time()

# 1) Import settings
exec(open(INFile).read())

mesh = MESH_CLASS(   WEDGE,   AXIAL_POINTS, RADIAL_POINTS, TANGENTIAL_POINTS, BOUNDARY_LAYER_COUNT,   BOUNDARY_LAYER_THICKNESS,
                 SECTIONS_PER_DEGREE, LE_FACTOR , TE_FACTOR , INLET_FACTOR,   OUTLET_FACTOR,   BLADE_FACTOR,
                 PLOT_MESH,   ADJOINT,   SOLVE,  CFX_POSTPROCESS,  BFM_POSTPROCESS)

              

if GEOMETRY == "DESIGN":

    # Mach definition-------------------------------
    # settings = DESIGN_SETTINGS(MACHINE_NAME,N_STAGE,N_ROWS,dim,TYPE,m_dot,beta_TT,M_in,V_in,P_in,T_in,rho_in,Pt_in,Tt_in,Rm,c,sigma,s,tip_cl,t_max,shape,N,R,gamma,Cp,Tcrit,Pcrit,H,
    #                  alpha1,alpha3,ki,phi,eta_TT_stage,eta_TT_r,beta_r,r_gap,s_gap,twist,QO_LE_R,QO_TE_R,QO_LE_S,QO_TE_S,
    #                  PLOT_BLADE,MESH_BFM,MESH_BLADE,Y_PLUS)
    #  del (MACHINE_NAME,N_STAGE,N_ROWS,dim,TYPE,m_dot,beta_TT,M_in,V_in,P_in,T_in,rho_in,Pt_in,Tt_in,Pcrit,Tcrit,Rm,c,sigma,s,tip_cl,t_max,shape,N,R,gamma,Cp,H,alpha1,alpha3,ki,phi,
    #  eta_TT_stage,eta_TT_r,r_gap,s_gap,twist,QO_LE_R,QO_TE_R,QO_LE_S,QO_TE_S,PLOT_BLADE,MESH_BFM,MESH_BLADE,Y_PLUS,WEDGE,AXIAL_POINTS,
    #  BOUNDARY_LAYER_COUNT,BOUNDARY_LAYER_THICKNESS,SECTIONS_PER_DEGREE,INLET_FACTOR,OUTLET_FACTOR,BLADE_FACTOR,PLOT_MESH,
    #  ADJOINT,SOLVE,POSTPROCESS)
    #                  #-------------------------------------
    # Coefficient definition---------------------------------
    settings = DESIGN_SETTINGS(MACHINE_NAME,N_STAGE,N_ROWS,dim,TYPE,m_dot,beta_TT,Pt_in,Tt_in,Rm,c,sigma,s,tip_cl,
                 t_max,shape,N,R,gamma,Cp,Tcrit,Pcrit,H,alpha1,alpha3,ki,
                 phi,psi,eta_TT_stage,eta_TT_r,beta_r,r_gap,s_gap,twist,QO_LE_R,QO_TE_R,QO_LE_S,QO_TE_S,
                 PLOT_BLADE,MESH_BFM,BFM_MODEL,MESH_BLADE,SENSITIVITY,OPTIMIZATION,PLOT_COMPARISON,Y_PLUS)

    del (MACHINE_NAME,N_STAGE,N_ROWS,dim,TYPE,m_dot,beta_TT,Pt_in,Tt_in,Pcrit,Tcrit,Rm,c,sigma,s,tip_cl,t_max,shape,N,R,gamma,Cp,H,alpha1,alpha3,ki,phi,
            eta_TT_stage,eta_TT_r,r_gap,s_gap,twist,QO_LE_R,QO_TE_R,QO_LE_S,QO_TE_S,PLOT_BLADE,MESH_BFM,BFM_MODEL,MESH_BLADE,SENSITIVITY,PLOT_COMPARISON,Y_PLUS,WEDGE,AXIAL_POINTS, RADIAL_POINTS,
            BOUNDARY_LAYER_COUNT,BOUNDARY_LAYER_THICKNESS,SECTIONS_PER_DEGREE,INLET_FACTOR,OUTLET_FACTOR,BLADE_FACTOR,PLOT_MESH,
            ADJOINT,SOLVE,CFX_POSTPROCESS,BFM_POSTPROCESS)
        

   

    settings=DESIGN_GEOMETRY(mesh,settings)


if GEOMETRY == "EXISTING":

    settings = EXISTING_SETTINGS(MACHINE_NAME,N_STAGE,N_ROWS,dim,TYPE,Pt_in,Tt_in,P_out,RPM,
                 R,gamma,Cp,Tcrit,Pcrit,T_in,P_in,rho_in,M_in,V_in,PLOT_BLADE,MESH_BFM,BFM_MODEL,MESH_BLADE,SENSITIVITY,OPTIMIZATION,PLOT_COMPARISON,Y_PLUS)

    # EXISTING_GEOMETRY(mesh,settings)


#---------N.B. inside this I set n_row=1 to study just the rotor-------------#
generate_P2BFM_input(settings,mesh)
#-----------------------------------------------#


P2BFM = os.environ["P2BFM"]
file="P2BFM_"+str(settings.MACHINE_NAME)+".cfg"
generate_BFM_cfg(P2BFM,settings,mesh)
# Meangen2BFM.py /home/gabbo/Documents/bitbucket_local/fan-stage-design/M2P.cfg

if settings.SENSITIVITY == 'NO' and settings.OPTIMIZATION == 'NO' :
    os.chdir(P2BFM)
    os.system("python -m Parablade2BFM "+file)

if settings.SENSITIVITY == 'YES':
    sens_folder = P2BFM+"\\Sensitivity_Analysis"
    if os.path.isdir(sens_folder):
        shutil.rmtree(sens_folder)
    os.mkdir(sens_folder)
    generate_SA_template(sens_folder,P2BFM+"\\testcases\\"+str(settings.MACHINE_NAME)+"\\Stage_1\\Bladerow_1\\Bladerow.cfg")
    generate_SA_driver(sens_folder,file)
    generate_SA_input(sens_folder)
    # shutil.copy(HOME+"\\templates\\dakota_BFM_sensitivity.in",sens_folder+"\\dakota_BFM_sensitivity.in")
    
    os.chdir(sens_folder)
    os.system("dakota -i dakota_BFM_sensitivity.in -o uscita.out")
    os.system("dakota_restart_util to_tabular dakota.rst BFM_results.txt")
    
    # Now the call to P2BFM is done inside the driver 

if settings.OPTIMIZATION == 'YES':
    opt_folder = P2BFM+"\\Optimization"
    if os.path.isdir(opt_folder):
        shutil.rmtree(opt_folder)
    os.mkdir(opt_folder)
    generate_OPT_template(opt_folder,P2BFM+"\\testcases\\"+str(settings.MACHINE_NAME)+"\\Stage_1\\Bladerow_1\\Bladerow.cfg")
    generate_OPT_driver(opt_folder,file)
    generate_OPT_input(opt_folder)
    # shutil.copy(HOME+"\\templates\\dakota_BFM_sensitivity.in",sens_folder+"\\dakota_BFM_sensitivity.in")
    
    time
    os.chdir(opt_folder)
    os.system("dakota -i dakota_BFM_optimization.in -o uscita.out")
    os.system("dakota_restart_util to_tabular dakota.rst BFM_results.txt")

    


