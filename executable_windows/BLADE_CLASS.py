
import numpy as np
import math

# from settings import *
from LIEBLEIN import *
from calculators import *

class blade_shape:
    
    def __init__(self,row,criteria,settings,state1,state2):

        #  # Inport variables -----------------------------------------------------------------------#
        shape = settings.shape
        t_max = settings.t_max
        sigma = settings.sigma
        # #-----------------------------------------------------------------------------------------#

        # Compute flow deflection
        if row == "rotor":
            # beta1=abs(state1.beta)
            # beta2=abs(state2.beta)
            beta1=state1.beta
            beta2=state2.beta
            delta_beta =  beta2-beta1
            
            # eps = beta2-beta1
            # theta = eps                               # Initial blade deviation assumption, must be iterated
            theta = 12
            j=0
            if criteria == "lieblein":
                err=1
                while err >1e-5:
                    i,delta = lieblein_relations(shape,sigma,t_max,abs(beta1),theta)

                    eps = theta+i-delta
                    err = (delta_beta-eps)
                    theta = theta+err
                    # print("theta is ",theta)
                    # theta_new = eps+delta-i
                    # err = abs(theta_new - theta)
                    # theta = theta_new
                    j+=1
            self.eps = eps
            self.theta = theta
            self.i = i
            self.delta = delta
            self.beta_blade1=beta1-i
            self.beta_blade2=beta2+delta



        
        if row == "stator":
            alpha1=state1.alpha
            alpha2=state2.alpha
            delta_alpha =  alpha2-alpha1
            theta = 10
            # eps = alpha2-alpha1
            # theta = eps                               # Initial blade deviation assumption, must be iterated
            j=0
            if criteria == "lieblein":
                err=1
                while err >1e-5:
                    i,delta = lieblein_relations(shape,sigma,t_max,abs(alpha1),theta)

                    # since the stator is at the end of the compressor it must rectify the flow
                    # delta = abs(delta)
                    
                    eps = theta+i-delta
                    err = (delta_alpha-eps)
                    theta = theta+err
                    
                    # theta_new = eps+delta-i
                    # err = abs(theta_new - theta)
                    # theta = theta_new
                    j+=1
            self.eps = eps
            self.theta = theta
            self.i = i
            self.delta = delta
            self.alpha_blade1=alpha1-i
            self.alpha_blade2=alpha2+delta


        # Define B.L. parameters
        self.mu = getmu(state1.T)
        self.ds , self.Re = getY(state1.rho,self.mu,state1.V,settings.b,settings.Y_PLUS,"external")


            
        
        

        

    

       
        











   