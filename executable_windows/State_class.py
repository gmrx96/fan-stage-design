#from _typeshed import Self
import numpy as np
import math

from compressor_losses import Losses


#from __main__ import Cp


class State:
    
    def __init__(self):
        
        self.U = 0
        self.Va = 0 
        self.Vt = 0
        self.V = 0
        self.alpha = 0
        self.Wa = 0
        self.Wt = 0
        self.Wa = 0
        self.beta = 0
        self.W = 0

        self.Tt = 0
        self.Pt = 0
        self.T = 0
        self.P = 0
        self.rho = 0
        self.M = 0
        self.M_r = 0
        self.Pt_r = 0
        self.Tt_r = 0
        
        
        
    
    def vel_triangles(self,Va,Vt,U):
        
        self.U = U
        self.Va = Va 
        self.Vt = Vt
        self.V = np.sqrt(self.Va**2 + self.Vt**2)
        self.alpha = math.degrees(np.arctan(Vt/Va))
        self.Wa = Va
        self.Wt = Vt - U
        self.Wa = Va
        self.beta = math.degrees(np.arctan(self.Wt/self.Wa))
        self.W = np.sqrt(self.Wa**2 + self.Wt**2)

    def thermodynamic_1 (self,Tt,Pt,Cp,gamma,R):
       self.Tt = Tt
       self.Pt = Pt
       self.T = self.Tt - 0.5*(self.V**2)/Cp
       self.P = self.Pt*(self.T/self.Tt)**(gamma/(gamma-1))
       self.rho = self.P/(R*self.T)
       self.M = self.V/np.sqrt(gamma*R*self.T)
       self.M_r = self.W/np.sqrt(gamma*R*self.T)
       self.Tt_r = self.T + 0.5*(self.W**2)/Cp
       self.Pt_r = self.P + 0.5*self.rho*self.W**2


    def thermodynamic_2 (self,state1,L_r,eta_r,Cp,gamma,R):
        self.Tt = L_r/Cp + state1.Tt                                             # I don't have Tt2 from beginning, it can be found from real work
        self.T = self.Tt - 0.5*(self.V**2)/Cp

        self.Tt_is = eta_r*(self.Tt-state1.Tt) + state1.Tt
        self.T_is = self.Tt_is - 0.5*(self.V**2)/Cp

        self.Tt_r_corretta = state1.Tt_r
        self.T_corretta = self.Tt_r_corretta - 0.5*(self.W**2)/Cp
        


        # self.T_is = eta_r*(self.T-state1.T) + state1.T                          # Tt2_is can be found from efficiency relation
        # self.Tt_is = self.T_is + 0.5*(self.V**2)/Cp                             # Get Tt2_is correspondant to point 2'
        
        self.P= state1.Pt * (self.T_is/state1.Tt)**(gamma/(gamma-1))            # P2 from isoentropic relation
        self.rho = self.P/(R*self.T)
        self.M = self.V/np.sqrt(gamma*R*self.T)
        self.M_r = self.W/np.sqrt(gamma*R*self.T)
        self.Pt = self.P + 0.5*self.rho*self.V**2 
        self.Tt_r = self.T + 0.5*(self.W**2)/Cp
        self.Pt_r = self.P + 0.5*self.rho*self.W**2

        self.Pt_r_corretta = state1.Pt_r
        self.P_corretta = self.Pt_r_corretta - 0.5*self.rho*self.W**2
        self.P_corretta2= state1.Pt * (self.T_corretta/state1.Tt)**(gamma/(gamma-1))      
       




    def thermodynamic_3 (self,settings,state1,state2,eta_tt,R,Cp,gamma):

                      
        # self.Tt_is = eta_tt*(state2.Tt_is - state1.Tt) + state1.Tt                # Get Tt3_is from stage tota-to-total stage efficiency
        self.Tt_is = state2.Tt_is
        self.T_is = self.Tt_is - 0.5*(self.V**2)/Cp
        self.Tt = state2.Tt                                                       # In the stator the total enthalpy is conserved
        self.T = self.Tt - 0.5*(self.V**2)/Cp
        self.Pt = state1.Pt * (self.Tt_is/state1.Tt)**(gamma/(gamma-1))           # Pt3 from isoentropic relations
        self.P = self.Pt *(self.T_is/self.Tt_is)**(gamma/(gamma-1))               # Also P3 from isoentropic relations

        self.rho = self.P/(R*self.T)
        self.M = self.V/np.sqrt(gamma*R*self.T)
        self.M_r = self.W/np.sqrt(gamma*R*self.T)
       
        self.Tt_r = self.T + 0.5*(self.W**2)/Cp
        self.Pt_r = self.P + 0.5*self.rho*self.W**2
             


    # def B_L(self,settings):
    #     self.mu = getmu(self.T)
    #     self.ds , self.Re = getY(self.rho,self.mu,self.V,settings.b,settings.Y_PLUS,"external")
