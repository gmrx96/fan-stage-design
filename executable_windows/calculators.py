import numpy as np



def getmu(T):
    # This function computes the air dynamic viscosity according to Sutherland equation
    mu = (1.458e-6 * T**1.5) / (T+110.4)
    return mu



def getY(rho,mu,v,l,Y,flow):
    # This function computes the height of the first layer of the B.L. 
    Re = rho*v*l/mu
    if flow=="external":
        Cf = (2*np.log10(Re)-0.65)**(-2.3)     #Schlichting (1979)
    elif flow == "pipe":
        Cf = 2*((8/Re)**12 + ((2.457*np.log((Re/7)**0.9))**16 + (37530/Re)**16)**(-1.5))**(1/12)        #Churchill (1977)
    
    T = 0.5*Cf*rho*v**2
    Uf = (T/rho)**0.5
    ds = Y*mu/(Uf*rho)
    return ds,Re



# a,b=getY(1.225,0.000018,1,1,1,"external")
# c,f= getY(1.225,0.000018,1,1,1,"pipe")
# print(a,b)
# print(c)
# d=(a+c)/2
# print(d)
