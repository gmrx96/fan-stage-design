


class MESH_CLASS:
    
    
    def __init__(self,   WEDGE,   AXIAL_POINTS, RADIAL_POINTS, TANGENTIAL_POINTS, BOUNDARY_LAYER_COUNT,   BOUNDARY_LAYER_THICKNESS,
                 SECTIONS_PER_DEGREE, LE_FACTOR , TE_FACTOR , INLET_FACTOR,   OUTLET_FACTOR,   BLADE_FACTOR,
                 PLOT_MESH,   ADJOINT,   SOLVE, CFX_POSTPROCESS, BFM_POSTPROCESS):

        self.WEDGE = WEDGE
        self.AXIAL_POINTS = AXIAL_POINTS
        self.RADIAL_POINTS = RADIAL_POINTS
        self.TANGENTIAL_POINTS = TANGENTIAL_POINTS
        self.BOUNDARY_LAYER_COUNT = BOUNDARY_LAYER_COUNT
        self.BOUNDARY_LAYER_THICKNESS = BOUNDARY_LAYER_THICKNESS
        self.SECTIONS_PER_DEGREE = SECTIONS_PER_DEGREE
        self.LE_FACTOR = LE_FACTOR
        self.TE_FACTOR = TE_FACTOR
        self.INLET_FACTOR = INLET_FACTOR
        self.OUTLET_FACTOR = OUTLET_FACTOR
        self.BLADE_FACTOR = BLADE_FACTOR
        self.PLOT_MESH = PLOT_MESH
        self.ADJOINT = ADJOINT
        self.SOLVE = SOLVE
        self.CFX_POSTPROCESS = CFX_POSTPROCESS
        self.BFM_POSTPROCESS = BFM_POSTPROCESS




# from settings import Y_PLUS
class DESIGN_SETTINGS:
    
    # Mac definition-------------------------------------------------------
    # def __init__(self,MACHINE_NAME,N_STAGE,N_ROWS,dim,TYPE,m_dot,beta_TT,M_in,V_in,P_in,T_in,rho_in,Pt_in,Tt_in,Rm,c,sigma,s,tip_cl,
                #  t_max,shape,N,R,gamma,Cp,Tcrit,Pcrit,H,alpha1,alpha3,ki,
                #  phi,eta_TT_stage,eta_TT_r,beta_r,r_gap,s_gap,twist,QO_LE_R,QO_TE_R,QO_LE_S,QO_TE_S,
                #  PLOT_BLADE,MESH_BFM,MESH_BLADE,Y_PLUS):
    #-------------------------------------------------------------
    # Coefficient definition
    def __init__(self,MACHINE_NAME,N_STAGE,N_ROWS,dim,TYPE,m_dot,beta_TT,Pt_in,Tt_in,Rm,c,sigma,s,tip_cl,
                 t_max,shape,N,R,gamma,Cp,Tcrit,Pcrit,H,alpha1,alpha3,ki,
                 phi,psi,eta_TT_stage,eta_TT_r,beta_r,r_gap,s_gap,twist,QO_LE_R,QO_TE_R,QO_LE_S,QO_TE_S,
                 PLOT_BLADE,MESH_BFM,BFM_MODEL,MESH_BLADE,SENSITIVITY,OPTIMIZATION,PLOT_COMPARISON,Y_PLUS):
        

        # # Mach definition----------------------------
        # self.V_in = V_in
        # self.M_in = M_in
        # self.P_in = P_in
        # self.T_in = T_in
        # self.rho_in = rho_in
        # #-----------------------------------------------

        # Coefficient definition
        self. psi = psi
        #-----------------------------------------------

        self.MACHINE_NAME = MACHINE_NAME
        self.N_STAGE = N_STAGE
        self.N_ROWS = N_ROWS
        self.dim = dim
        self.TYPE = TYPE
        self.m_dot = m_dot
        self.beta_tt = beta_TT
        self.Pt_in = Pt_in
        self.Tt_in = Tt_in
        self.Rm = Rm
        self.c = c
        self.sigma = sigma 
        self.s = s
        self.tip_cl = tip_cl
        self.t_max = t_max
        self.shape = shape
        self.N = N
        self.R = R
        self.gamma = gamma
        self.Cp = Cp
        self.Tcrit=Tcrit
        self.Pcrit=Pcrit
        self.H = H
        self.alpha1 = alpha1
        self.alpha3 = alpha3
        self.ki = ki
        self.phi = phi
        self.eta_TT_stage = eta_TT_stage
        self.eta_TT_r = eta_TT_r
        self.beta_r=beta_r
        self.r_gap = r_gap
        self.s_gap = s_gap
        self.twist = twist
        self.QO_LE_R = QO_LE_R 
        self.QO_TE_R = QO_TE_R 
        self.QO_LE_S = QO_LE_S 
        self.QO_TE_S = QO_TE_S 
        self.PLOT_BLADE = PLOT_BLADE
        self.MESH_BFM = MESH_BFM
        self.BFM_MODEL= BFM_MODEL
        self.MESH_BLADE = MESH_BLADE
        self.SENSITIVITY = SENSITIVITY
        self.OPTIMIZATION = OPTIMIZATION
        self.PLOT_COMPARISON = PLOT_COMPARISON
        self.Y_PLUS = Y_PLUS

    

class EXISTING_SETTINGS:

    def __init__(self,MACHINE_NAME,N_STAGE,N_ROWS,dim,TYPE,Pt_in,Tt_in,P_out,RPM,
                 R,gamma,Cp,Tcrit,Pcrit,T_in,P_in,rho_in,M_in,V_in,PLOT_BLADE,MESH_BFM,BFM_MODEL,MESH_BLADE,SENSITIVITY,OPTIMIZATION,PLOT_COMPARISON,Y_PLUS):


        self.MACHINE_NAME = MACHINE_NAME
        self.N_STAGE = N_STAGE
        self.N_ROWS = N_ROWS
        self.dim = dim
        self.TYPE = TYPE
        self.Pt_in= Pt_in
        self.Tt_in = Tt_in
        self.P_out = P_out
        self.RPM = RPM
        self.R = R
        self.gamma = gamma
        self.Cp = Cp
        self.Tcrit=Tcrit
        self.Pcrit=Pcrit
        self.T_in = T_in
        self.P_in = P_in
        self.rho_in = rho_in
        self.M_in = M_in
        self.V_in = V_in
        self.PLOT_BLADE = PLOT_BLADE
        self.MESH_BFM = MESH_BFM
        self.BFM_MODEL= BFM_MODEL
        self.MESH_BLADE = MESH_BLADE
        self.SENSITIVITY = SENSITIVITY
        self.OPTIMIZATION = OPTIMIZATION
        self.PLOT_COMPARISON = PLOT_COMPARISON
        self.Y_PLUS = Y_PLUS

    













