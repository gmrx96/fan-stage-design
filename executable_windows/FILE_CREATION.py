
import os
import numpy as np
# from settings import *
from datetime import date
import re

TEMP_FOLDER = "C:\\Users\\GMrx1\\Desktop\\bitbucket\\fan-stage-design\\templates"




def generate_P2BFM_input(settings,mesh):
    RPM = settings.RPM
    #-----------------------------------------------------------------------------------------#
    P2BFM = os.environ["P2BFM"]
    filename = P2BFM+"\\templates\\P2BFM_"+str(settings.MACHINE_NAME)
    # filename=TEMP_FOLDER+"\\P2BFM_"+str(settings.MACHINE_NAME)
    f= open(filename+".cfg","w+")
    f.write("# ---------------------------------------------------------------------------- #\n")
    f.write("# --------------Parablade2BFM configuration file -------------- #\n")
    f.write("# ---------------------------------------------------------------------------- #\n\n")
    f.write("\n")
    f.write("MACHINE_NAME = "+str(settings.MACHINE_NAME)+"\n")
    f.write("\n")
    f.write("# ---------------------------------------------------------------------------- #\n")
    f.write("# -------------------------- Design point --------------------- #\n")
    f.write("# ---------------------------------------------------------------------------- #\n")
    f.write("N_stage = "+str(settings.N_STAGE)+"\n")
    f.write("\n")
    f.write("N_rows = "+str(settings.N_ROWS)+"\n")
    f.write("\n")
    f.write("N_dim = "+str(settings.dim)+"\n")
    f.write("\n")
    f.write("TYPE = "+str(settings.TYPE)+"\n")
    f.write("\n")
    if RPM >= 0:
        f.write("Omega =  -"+str(RPM)+"\n")
    else:
        f.write("Omega =  "+str(abs(RPM))+"\n")
    
    f.write("\n")
    f.write("R_gas = "+str(settings.R)+"\n")
    f.write("\n")
    f.write("gamma = "+str(settings.gamma)+"\n")
    f.write("\n")
    f.write("P_t_in = "+str(settings.Pt_in)+"\n")
    f.write("\n")
    f.write("T_t_in = "+str(settings.Tt_in)+"\n")
    f.write("\n")
    f.write("P_s_out = "+str(settings.P_out)+"\n")
    f.write("\n")
    f.write("PLOT_BLADE = "+str(settings.PLOT_BLADE)+"\n")
    f.write("\n")
    f.write("MESH_BFM = "+str(settings.MESH_BFM)+"\n")
    f.write("\n")
    f.write("MESH_BLADE = "+str(settings.MESH_BFM)+"\n")
    f.write("\n")
    f.write("SENSITIVITY = "+str(settings.SENSITIVITY)+"\n")
    f.write("\n")
    f.write("OPTIMIZATION = "+str(settings.OPTIMIZATION)+"\n")
    f.write("\n")
    f.write("PLOT_COMPARISON = "+str(settings.PLOT_COMPARISON)+"\n")
    f.write("\n")
    f.write("\n")
    f.write("# ---------------------------------------------------------------------------- #\n")
    f.write("# ----------------------Mesh parameters------------------------------- #\n")
    f.write("## ---------------------------------------------------------------------------- #\n")
    f.write("\n")
    f.write("WEDGE = "+str(mesh.WEDGE)+"\n")
    f.write("\n")
    f.write("AXIAL_POINTS = "+str(mesh.AXIAL_POINTS)+"\n")
    f.write("\n")
    f.write("RADIAL_POINTS = "+str(mesh.RADIAL_POINTS)+"\n")
    f.write("\n")
    f.write("TANGENTIAL_POINTS = "+str(mesh.TANGENTIAL_POINTS)+"\n")
    f.write("\n")
    f.write("BOUNDARY_LAYER_COUNT = "+str(mesh.BOUNDARY_LAYER_COUNT)+"\n")
    f.write("\n")
    f.write("BOUNDARY_LAYER_THICKNESS = "+str(mesh.BOUNDARY_LAYER_THICKNESS)+"\n")
    f.write("\n")
    f.write("SECTIONS_PER_DEGREE = "+str(mesh.SECTIONS_PER_DEGREE)+"\n")
    f.write("\n")
    f.write("LE_FACTOR = "+str(mesh.LE_FACTOR)+"\n")
    f.write("\n")
    f.write("TE_FACTOR = "+str(mesh.TE_FACTOR)+"\n")
    f.write("\n")
    f.write("INLET_FACTOR = "+str(mesh.INLET_FACTOR)+"\n")
    f.write("\n")
    f.write("OUTLET_FACTOR = "+str(mesh.OUTLET_FACTOR)+"\n")
    f.write("\n")
    f.write("BLADE_FACTOR = "+str(mesh.BLADE_FACTOR)+"\n")
    f.write("\n")
    f.write("PLOT_MESH = "+str(mesh.PLOT_MESH)+"\n")
    f.write("\n")
    f.write("ADJOINT = "+str(mesh.ADJOINT)+"\n")
    f.write("\n")
    f.write("SOLVE = "+str(mesh.SOLVE)+"\n")
    f.write("\n")
    f.write("CFX_POSTPROCESS = "+str(mesh.CFX_POSTPROCESS)+"\n")
    f.write("\n")
    f.write("BFM_POSTPROCESS = "+str(mesh.BFM_POSTPROCESS)+"\n")

    
    





def generate_M2P_input_2BLADES(settings,mesh,state1,state3,state2_losses,state3_losses,Blade1,Blade2):

 # Inport variables -----------------------------------------------------------------------#
    MACHINE_NAME = settings.MACHINE_NAME
    N_STAGE = settings.N_STAGE
    N_ROWS = settings.N_ROWS
    R = settings.R
    gamma = settings.gamma
    Pt1 = settings.Pt_in * 1e-5
    Tt1 = settings.Tt_in
    RPM = state1.U /settings.Rm* 60/2/np.pi
    m_dot = settings.m_dot
    N = settings.N
    P3 = state3.P
    ki = settings.ki
    phi = settings.phi
    psi = settings.psi
    Rm = settings.Rm
    c = settings.c
    b = settings.b
    tip_cl = settings.tip_cl
    r_gap = settings.r_gap
    s_gap = settings.s_gap
    # fblock_LE = state2_losses.fblock
    # fblock_TE = state3_losses.fblock
    eta_tt = settings.eta_TT_stage
    delta1 = Blade1.delta
    delta2 = Blade2.delta
    i1 = Blade1.i
    i2 = Blade2.i
    # t_max=settings.t_max
    dim = settings.dim
    twist = settings.twist
    QO_LE_R = settings.QO_LE_R 
    QO_TE_R = settings.QO_TE_R 
    QO_LE_S = settings.QO_LE_S 
    QO_TE_S = settings.QO_TE_S
    PLOT_BLADE = settings.PLOT_BLADE
    MESH_BFM = settings.MESH_BFM
    MESH_BLADE = settings.MESH_BLADE

    WEDGE = mesh.WEDGE
    AXIAL_POINTS = mesh.AXIAL_POINTS
    BOUNDARY_LAYER_COUNT = mesh.BOUNDARY_LAYER_COUNT
    BOUNDARY_LAYER_THICKNESS = mesh.BOUNDARY_LAYER_THICKNESS
    SECTIONS_PER_DEGREE = mesh.SECTIONS_PER_DEGREE
    INLET_FACTOR = mesh.INLET_FACTOR
    OUTLET_FACTOR = mesh.OUTLET_FACTOR
    BLADE_FACTOR = mesh.BLADE_FACTOR
    PLOT_MESH = mesh.PLOT_MESH
    ADJOINT = mesh.ADJOINT
    SOLVE = mesh.SOLVE
    BFM_POSTPROCESS = mesh.BFM_POSTPROCESS
    CFX_POSTPROCESS = mesh.CFX_POSTPROCESS





    #-----------------------------------------------------------------------------------------#
    # M2BFM = os.environ["M2BFM"]
    # filename = M2BFM+"\\templates\\M2P_"+str(name)
    filename=TEMP_FOLDER+"\\M2P_"+str(MACHINE_NAME)
    f= open(filename+".cfg","w+")
    f.write("# ---------------------------------------------------------------------------- #\n")
    f.write("# --------------Meangen2Parablade configuration file -------------- #\n")
    f.write("# ---------------------------------------------------------------------------- #\n\n")
    
    f.write("MACHINE_NAME = "+str(MACHINE_NAME)+"\n")



    f.write("# Number of stages in the machine(integer)\n")
    f.write("# Values for duty coefficients and blade shape\n")
    f.write("# should have same dimension as number of\n")
    f.write("# stages\n")
    f.write("N_stage = "+str(N_STAGE)+"\n\n")

    f.write("N_ROWS = "+str(N_ROWS)+"\n\n")

    f.write("# Axial machine type :: C for compressor :: T for turbine\n")
    f.write("TYPE = C\n\n")

    f.write("# Mass flow rate [kg s^-1]\n")
    f.write("mass_flow = "+str(m_dot)+"\n\n")

    f.write("# Rotation speed[rpm]\n")
    # if RPM >= 0:
    #     f.write("Omega =  -"+str(RPM)+"\n")
    # else:
    #     f.write("Omega =  "+str(abs(RPM))+"\n")
    f.write("Omega = "+str(RPM)+"\n\n")

    f.write("Rotation_axis = 1.0, 0.0, 0.0\n\n")

    f.write("# Working fluid gas constant[J kg^-1 K^-1]\n")
    f.write("R_gas = "+str(R)+"\n\n")

    f.write("# Working fluid specific heat ratio\n")
    f.write("gamma = "+str(gamma)+"\n\n")

    f.write("# Inlet total pressure[bar]\n")
    f.write("P_t_in = "+str(Pt1)+"\n\n")
    
    f.write("# Inlet total temperature[K]\n")
    f.write("T_t_in = "+str(Tt1)+"\n\n")

    f.write("P_s_out = "+str(P3)+"\n\n")

    f.write("# Number of Dimensions :: 2 :: 3\n")
    f.write("# Set NDIM = 2 for two-dimensional problems\n")
    f.write("# Set NDIM = 3 for three-dimensional problems\n")
    f.write("N_dim = "+str(dim)+"\n\n")

    f.write("# Rotor blade count for each stage(integer)\n")
    f.write("N_blade_R = "+str(N)+"\n\n")

    f.write("# Stator blade count for each stage(integer)\n")
    f.write("N_blade_S = "+str(N)+"\n\n")

    f.write("# Degree of reaction\n")
    f.write("R = "+str(ki)+"\n\n")

    f.write("# Flow coefficient\n")
    f.write("phi = "+str(phi)+"\n\n")

    f.write("# Work coefficient\n")
    f.write("psi = "+str(psi)+"\n\n")

    f.write("# Mean radius[m]\n")
    f.write("r_m = "+str(Rm)+"\n\n")

    f.write("# Rotor tip gap, normalized with respect to blade span\n")
    f.write("ROTOR_TIP_GAP = "+str(tip_cl/b)+"\n\n")

    f.write("# Rotor chord lengths[m]\n")
    f.write("chord_R = "+str(c)+"\n\n")

    f.write("# Stator chord lengths[m]\n")
    f.write("chord_S = "+str(c)+"\n\n")

    f.write("# Normalized row gap wrt first blade row\n")
    f.write("# chord length.\n")
    f.write("rowGap = "+str(r_gap)+"\n\n")

    f.write("# Normalized stage gap wrt first blade row\n")
    f.write("# chord length.\n")
    f.write("stageGap = "+str(s_gap)+"\n\n")

    f.write("# Blade twist value(>0)\n")
    f.write("twist = "+str(twist)+"\n\n")

    f.write("# Rotor blade row leading edge sweep angle[deg]\n")
    f.write("QO_LE_R = "+str(QO_LE_R)+"\n")
    f.write("# Rotor blade row trailing edge sweep angle[deg]\n")
    f.write("QO_TE_R = "+str(QO_TE_R)+"\n")
    f.write("# Stator blade row leading edge sweep angle[deg]\n")
    f.write("QO_LE_S = "+str(QO_LE_S)+"\n")
    f.write("# Stator blade row trailing edge sweep angle[deg]\n")
    f.write("QO_TE_S = "+str(QO_TE_S)+"\n")

    f.write("# Rotor incidence angle[deg]\n")
    f.write("dev_R_LE = "+str(i1)+"\n\n")
    # f.write("dev_R_LE = 90\n\n")

    f.write("# Rotor deviation angle[deg]\n")
    f.write("dev_R_TE = "+str(delta1)+"\n\n")
    # f.write("dev_R_TE = 0\n\n")

    f.write("# Stator incidence angle[deg]\n")
    f.write("dev_S_LE = "+str(i2)+"\n\n")
    # f.write("dev_S_LE = 0\n\n")


    f.write("# Stator deviation angle[deg]\n")
    f.write("dev_S_TE = "+str(delta2)+"\n\n\n")
    # f.write("dev_S_TE = 0\n\n\n")

  

#     # --------------------------Initial geometry as rotor67-------------------------------#
#     f.write("t_le_R = 0.0006693359494975922, 0.0018928674557306228, 0.0019449153991979709, 0.0008739350479142291\n")
#     f.write("t_te_R = 0.0014997139991203107, 0.00010000000000000219, 0.00010000000000000166, 0.0004386091425299807\n")
#     f.write("d_1_R = 0.3371973356939866, 0.2646650076925527, 0.332498891792417, 0.3613013989627863\n")
#     f.write("d_2_R = 0.36112611080317664, 0.32325392331261366, 0.5112249157259443, 0.4836145113992993\n")

#     f.write("T_1_R = 0.002546465411929295, 0.019075829292213346, 0.00010000000000000018, 0.02313819261938296\n")
#     f.write("T_2_R = 0.01779346609002048, 0.022536871245423685, 0.01975140550550291, 0.03503950879069074\n")
#     f.write("T_3_R = 0.042546699459077235, 0.02012814072142602, 0.01320391021275751, 0.02447215343886261\n")
#     f.write("T_4_R = 0.05353048483160095, 0.015679121952556684, 0.028827646857360504, 0.010367378479338652\n")
#     f.write("T_5_R = 0.03787517131212501, 0.00869029932624595, 0.00010000000000000308, 0.024266582511949936\n")
#     f.write("T_6_R = 0.019506134409369012, 0.020452763331955483, 0.00010000000000000216, 0.005654341050033827\n\n")

#    #This part is not important since it represents the stator

#     f.write("t_le_S = 0.0006693359494975922, 0.0018928674557306228, 0.0019449153991979709, 0.0008739350479142291\n")
#     f.write("t_te_S = 0.0014997139991203107, 0.00010000000000000219, 0.00010000000000000166, 0.0004386091425299807\n")
#     f.write("d_1_S = 0.3371973356939866, 0.2646650076925527, 0.332498891792417, 0.3613013989627863\n")
#     f.write("d_2_S = 0.36112611080317664, 0.32325392331261366, 0.5112249157259443, 0.4836145113992993\n")

#     f.write("T_1_S = 0.014055042472713537, 0.022552625184504422, 0.00010000000000000018, 0.00010000000000000021\n")
#     f.write("T_2_S = 0.0326815541481784, 0.04563970917975673, 0.0001, 0.00010000000000000071\n")
#     f.write("T_3_S = 0.040333843876396595, 0.08326935094861851, 0.005368580072629134, 0.032458763702131306\n")
#     f.write("T_4_S = 0.03619840036020486, 0.07497402804237907, 0.03879799161959874, 0.04185795862113147\n")
#     f.write("T_5_S = 0.03047033837772709, 0.04944264572312122, 0.046517691308877396, 0.03759410318533932\n")
#     f.write("T_6_S = 0.0004262488176859667, 0.023030461111436553, 0.013217793581095231, 0.023686849524389257\n\n\n")
#     #-----------------------------------------------------------------------------------------#

    

    #--------------------------GENERIC SHAPE---------------------------------------#
    f.write("t_le_R = 0.003, 0.003\n")
    f.write("t_te_R = 0.005, 0.003\n")
    f.write("d_1_R = 0.40, 0.40\n")

    f.write("d_2_R = 0.35, 0.35\n")
    f.write("T_1_R = 0.015, 0.015\n")
    f.write("T_2_R = 0.017, 0.017\n")
    f.write("T_3_R = 0.019, 0.019\n")
    f.write("T_4_R = 0.024, 0.024\n")
    f.write("T_5_R = 0.030, 0.030\n")
    f.write("T_6_R = 0.020, 0.020\n\n")
    # f.write("T_6_R = 0.080, 0.080\n\n")

    f.write("t_le_S = 0.003, 0.003\n")
    f.write("t_te_S = 0.005, 0.003\n")
    f.write("d_1_S = 0.40, 0.40\n")
    f.write("d_2_S = 0.35, 0.35\n")
    f.write("T_1_S = 0.015, 0.015\n")
    f.write("T_2_S = 0.017, 0.017\n")
    f.write("T_3_S = 0.019, 0.019\n")
    f.write("T_4_S = 0.024, 0.024\n")
    f.write("T_5_S = 0.030, 0.030\n")
    f.write("T_6_S = 0.020, 0.020\n\n\n")
   #-------------------------------------------------------------#



    f.write("# Guess of stage isentropic efficiency\n")
    f.write("eta_guess = "+str(eta_tt)+"\n\n")

    f.write("# Blade plot option for PlotBlade :: YES :: NO\n")
    f.write("PLOT_BLADE = "+PLOT_BLADE+"\n\n")
    
    f.write("# BFM mesh option :: YES :: NO\n")
    f.write("MESH_BFM = "+MESH_BFM+"\n\n")

    f.write("# BLADE mesh option :: YES :: NO\n")
    f.write("MESH_BLADE = "+MESH_BLADE+"\n\n")

    f.write("# ---------------------------------------------------------------------------- #\n")
    f.write("# ----------------------Mesh parameters------------------------------- #\n")
    f.write("## ---------------------------------------------------------------------------- #\n\n")

    f.write("# Wedge angle of 3D domain in degrees. Should be lower than 180 degrees.\n")
    f.write("WEDGE = "+str(WEDGE)+"\n\n")

    f.write("# Node count in axial direction within the blade zones. Should be an integer.\n")
    f.write("AXIAL_POINTS = "+str(AXIAL_POINTS)+"\n\n")

    f.write("# Number of boundary layers in the blade mesh.\n")
    f.write("BOUNDARY_LAYER_COUNT = "+str(BOUNDARY_LAYER_COUNT)+"\n\n")

    f.write("# Thickness of first boundary layer in blade mesh.\n")
    f.write("BOUNDARY_LAYER_THICKNESS = "+str(BOUNDARY_LAYER_THICKNESS)+"\n\n")

    f.write("# Number of sections per wedge degree. Should be an integer.\n")
    f.write("SECTIONS_PER_DEGREE = "+str(SECTIONS_PER_DEGREE)+"\n\n")

    f.write("# Cell size multiplication factor at the inlet.\n")
    f.write("INLET_FACTOR = "+str(INLET_FACTOR)+"\n\n")

    f.write("# Cell size multiplication factor at the outlet.\n")
    f.write("OUTLET_FACTOR = "+str(OUTLET_FACTOR)+"\n\n")

    f.write("# Cell size multiplication factor at the blade surface.\n")
    f.write("BLADE_FACTOR = "+str(BLADE_FACTOR)+"\n\n")

    f.write("# Mesh plot option. If 'YES', it will use the gmesh plot option to visualize the 3D mesh. If 'NO', it will skip this.\n")
    f.write("PLOT_MESH = "+PLOT_MESH+"\n\n")

    f.write("ADJOINT = "+ADJOINT+"\n\n")

    f.write("SOLVE = "+SOLVE+"\n\n")

    f.write("BFM_POSTPROCESS = "+BFM_POSTPROCESS+"\n\n")

    f.write("CFX_POSTPROCESS = "+CFX_POSTPROCESS+"\n\n")

    f.close()
    

    # if os.path.exists(filename+".cfg"):
    #     os.remove(filename+".cfg")
    # os.rename(filename+".txt",filename+".cfg")



#   ------------------------------------------------------------------------------------ #





def generate_BFM_cfg(loc,settings,mesh):
    
    
    # Inport variables
    elements_id = str(int(mesh.AXIAL_POINTS))+"x"+str(int(mesh.RADIAL_POINTS)) +"x"+str(int(mesh.TANGENTIAL_POINTS))
    R = settings.R
    gamma = settings.gamma
    Pt_in = settings.Pt_in
    Tt_in = settings.Tt_in
    T_in = settings.T_in
    P_in = settings.P_in
    rho_in = settings.rho_in
    M_in = settings.M_in
    Va_in = settings.V_in

    P_out = settings.P_out

    Tcrit=settings.Tcrit
    Pcrit=settings.Pcrit
    RPM = settings.RPM
    
    BFM_MODEL= settings.BFM_MODEL
    
    # filename = loc+"\\MESHOutput\\STAGE_BFM\\BFM_"+str(settings.MACHINE_NAME)+".cfg"
    filename = loc+"\\templates\\BFM_"+str(settings.MACHINE_NAME)+".cfg"
    BFM_input_filename = "BFM_"+str(settings.MACHINE_NAME)+"_geometry.drg"
    MESH_filename = "FAN_MESH"+elements_id+".cgns"
    f= open(filename,"w+")
    
    f.write(" %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% \n")
    f.write("%\n")
    f.write("% SU2 configuration file\n")
    f.write("% Case description: Fan BFM Analysis\n")
    f.write("% Author: G. Morvillo\n")
    f.write("% Institution: Delft University of Technology\n")
    f.write("% Date: "+str(date.today())+"\n")
    f.write("% \File Version 7.2.0 Blackbird\n")
    f.write("%\n")
    f.write("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n")
    f.write("%\n")
    f.write("%\n")


    f.write(" % ------------- DIRECT, ADJOINT, AND LINEARIZED PROBLEM DEFINITION ------------%\n")
    f.write("%\n")
    f.write("% Physical governing equations (EULER, NAVIER_STOKES,\n")
    f.write("%                               WAVE_EQUATION, HEAT_EQUATION, LINEAR_ELASTICITY,\n")
    f.write("%                               POISSON_EQUATION)\n")
    f.write("SOLVER= EULER\n")
    f.write("%\n")
    f.write("% Specify turbulent model (NONE, SA, SST)\n")
    f.write("KIND_TURB_MODEL= NONE\n")
    f.write("%\n")
    f.write("% Mathematical problem (DIRECT, ADJOINT, LINEARIZED)\n")
    f.write("MATH_PROBLEM= DIRECT\n")
    f.write("%\n")
    f.write("% Restart solution (NO, YES)\n")
    f.write("RESTART_SOL= NO\n")
    f.write("%\n")
    f.write("%\n")



    f.write("% -------------------- COMPRESSIBLE FREE-STREAM DEFINITION --------------------%\n")
    f.write("%\n")
    f.write("% Mach number (non-dimensional, based on the free-stream values)\n")
    f.write("MACH_NUMBER= "+str(M_in)+"\n")
    f.write("%\n")
    f.write("% Angle of attack (degrees, only for compressible flows)\n")
    f.write("AOA= 0.0\n")
    f.write("%\n")
    f.write("% Free-stream pressure (101325.0 N/m^2 by default, only Euler flows) \n")
    f.write("FREESTREAM_PRESSURE= "+str(P_in)+"\n")
    f.write("%\n")
    f.write("% Free-stream temperature (273.15 K by default)\n")
    f.write("FREESTREAM_TEMPERATURE=  "+str(T_in)+"\n")
    f.write("%\n")
    f.write("% Free-stream density (1.2886 Kg/m3 by default)\n")
    f.write("FREESTREAM_DENSITY= "+str(rho_in)+"\n")
    f.write("%\n")
    f.write("% Free-stream option to choose if you want to use Density (DENSITY_FS) or Temperature (TEMPERATURE_FS) to initialize the solution\n")
    f.write("FREESTREAM_OPTION= TEMPERATURE_FS\n")
    f.write("%\n")
    f.write("% Free-stream Turbulence Intensity\n")
    f.write("FREESTREAM_TURBULENCEINTENSITY = 0.03\n")
    f.write("%\n")
    f.write("% Free-stream Turbulent to Laminar viscosity ratio\n")
    f.write("FREESTREAM_TURB2LAMVISCRATIO = 100.0\n")
    f.write("%\n")
    f.write("%Init option to choose between Reynolds (default) or thermodynamics quantities for initializing the solution (REYNOLDS, TD_CONDITIONS)\n")
    f.write("INIT_OPTION= TD_CONDITIONS\n")
    f.write("%\n")




    f.write("% ---------------------- REFERENCE VALUE DEFINITION ---------------------------%\n")
    f.write("%\n")
    f.write("% Reference origin for moment computation\n")
    f.write("REF_ORIGIN_MOMENT_X = 0.00\n")
    f.write("REF_ORIGIN_MOMENT_Y = 0.00\n")
    f.write("REF_ORIGIN_MOMENT_Z = 0.25\n")
    f.write("%\n")
    f.write("% Reference length for pitching, rolling, and yawing non-dimensional moment\n")
    f.write("REF_LENGTH= 1.0\n")
    f.write("%\n")
    f.write("% Reference area for force coefficients (0 implies automatic calculation)\n")
    f.write("REF_AREA= 1.0\n")
    f.write("%\n")
    f.write("%\n")
    f.write("% Flow non-dimensionalization (DIMENSIONAL, FREESTREAM_PRESS_EQ_ONE,\n")
    f.write("%                              FREESTREAM_VEL_EQ_MACH, FREESTREAM_VEL_EQ_ONE)\n")
    f.write("REF_DIMENSIONALIZATION= DIMENSIONAL\n")
    f.write("MOTION_ORIGIN = 0 0 0\n")
    f.write("FREESTREAM_VELOCITY= ("+str(Va_in)+",0,0)\n")
    f.write("%\n")
    f.write("%\n")




    f.write("% ------------------------------ EQUATION OF STATE ----------------------------%\n")
    f.write("%\n")
    f.write("% Different gas model (STANDARD_AIR, IDEAL_GAS, VW_GAS, PR_GAS)\n")
    f.write("FLUID_MODEL= IDEAL_GAS\n")
    f.write("%\n")
    f.write("% Ratio of specific heats (1.4 default and the value is hardcoded\n")
    f.write("%                          for the model STANDARD_AIR)\n")
    f.write("GAMMA_VALUE= "+str(gamma)+"\n")
    f.write("%\n")
    f.write("% Specific gas constant (287.058 J/kg*K default and this value is hardcoded \n")
    f.write("%                        for the model STANDARD_AIR)\n")
    f.write("GAS_CONSTANT= "+str(R)+"\n")
    f.write("%\n")
    f.write("% Critical Temperature (273.15 K by default)\n")
    f.write("CRITICAL_TEMPERATURE= "+str(Tcrit)+"\n")
    f.write("%\n")
    f.write("% Critical Pressure (101325.0 N/m^2 by default)\n")
    f.write("CRITICAL_PRESSURE= "+str(Pcrit)+"\n")
    f.write("%\n")
    f.write("% Acentri factor (0.035 (air))\n")
    f.write("ACENTRIC_FACTOR= 0.035\n")
    f.write("%\n")
    f.write("%\n")




    f.write("% ----------------------- BODY FORCE DEFINITION -------------------------------%\n")
    f.write("%\n")
    f.write("% Apply a body force as a source term (NO, YES)\n")
    f.write("BODY_FORCE= YES\n")
    f.write("%\n")
    f.write("% Select what type of body force to use (CONSTANT_BF, VARIABLE_BF)\n")
    f.write("BODY_FORCE_TYPE= VARIABLE_BF\n")
    f.write("%\n")
    f.write("BFM_INPUT_FILENAME= "+BFM_input_filename+"\n")
    f.write("%\n")
    f.write("% Determine rotation for body force calculation in RPM (For 2D, negative if moving in the negative y-direction)\n")
    if RPM >= 0:
        f.write("BODY_FORCE_ROTATION= -"+str(RPM)+"\n")
    else:
        f.write("BODY_FORCE_ROTATION= "+str(abs(RPM))+"\n")
    f.write("%\n")
    f.write("BFM_FORMULATION= "+BFM_MODEL+"\n") 
    f.write("%\n")
    f.write("BFM_ROTATION_AXIS = 1.0, 0.0, 0.0\n")




    f.write("% --------------------------- VISCOSITY MODEL ---------------------------------%\n")
    f.write("%\n")
    f.write("% Viscosity model (SUTHERLAND, CONSTANT_VISCOSITY).\n")
    f.write("VISCOSITY_MODEL= SUTHERLAND\n")
    f.write("%\n")
    f.write("% Molecular Viscosity that would be constant (1.716E-5 by default)\n")
    f.write("MU_CONSTANT= 1.3764E-5\n")
    f.write("%\n")
    f.write("% Sutherland Viscosity Ref (1.716E-5 default value for AIR SI)\n")
    f.write("MU_REF= 1.716E-5\n")
    f.write("%\n")
    f.write("% Sutherland Temperature Ref (273.15 K default value for AIR SI)\n")
    f.write("MU_T_REF= 273.15\n")
    f.write("%\n")
    f.write("% Sutherland constant (110.4 default value for AIR SI)\n")
    f.write("SUTHERLAND_CONSTANT= 110.4\n")
    f.write("%\n")
    f.write("%\n")




    f.write("% --------------------------- THERMAL CONDUCTIVITY MODEL ----------------------%\n")
    f.write("%\n")
    f.write("% Conductivity model (CONSTANT_CONDUCTIVITY, CONSTANT_PRANDTL).\n")
    f.write("CONDUCTIVITY_MODEL= CONSTANT_PRANDTL\n")
    f.write("%\n")
    f.write("% Molecular Thermal Conductivity that would be constant (0.0257 by default)\n")
    f.write("%KT_CONSTANT= 0.047280\n")
    f.write("%\n")
    f.write("%\n")




    f.write("% -------------------- BOUNDARY CONDITION DEFINITION --------------------------%\n")
    f.write("%\n")
    f.write("% Navier-Stokes wall boundary marker(s) (NONE = no marker)\n")
    f.write("%\n")
    f.write("MARKER_EULER=(HUB_WALL, SHROUD_WALL)\n")
    f.write("%\n")
    f.write("% Periodic boundary marker(s) (NONE = no marker)\n")
    if mesh.WEDGE == 360:
        f.write("%\n")
    else:
        f.write("% Format: ( periodic marker, donor marker, rot_cen_x, rot_cen_y, rot_cen_z, rot_angle_x-axis, rot_angle_y-axis, rot_angle_z-axis, translation_x, translation_y, translation_z)\n")
        f.write("MARKER_PERIODIC= ( RIGHT_SYM, LEFT_SYM, 0.0, 0.0, 0.0, "+str(mesh.WEDGE)+", 0.0, 0.0, 0.0, 0.0, 0.0 )\n")
        f.write("%\n")
    f.write("SPATIAL_FOURIER= YES\n")
    f.write("%\n")




    f.write("%-------- INFLOW/OUTFLOW BOUNDARY CONDITION SPECIFIC FOR TURBOMACHINERY --------%\n")
    f.write("%\n")
    f.write("% Inflow and Outflow markers must be specified, for each blade (zone), following the natural groth of the machine (i.e, from the first blade to the last)\n")
    f.write("%\n")
    f.write("INLET_TYPE= TOTAL_CONDITIONS\n")
    f.write("MARKER_INLET=(INLET,"+str(Tt_in)+", "+str(Pt_in)+", 1.0, 0.0, 0.0)\n")
    f.write("%\n")
    f.write("MARKER_OUTLET= ( OUTLET, "+str(P_out)+" )\n")
    f.write("%\n")
    f.write("% ------------- COMMON PARAMETERS DEFINING THE NUMERICAL METHOD ---------------%\n")
    f.write("%\n")
    f.write("% Numerical method for spatial gradients (GREEN_GAUSS, WEIGHTED_LEAST_SQUARES)\n")
    f.write("NUM_METHOD_GRAD= GREEN_GAUSS\n")
    f.write("%\n")
    f.write("% Courant-Friedrichs-Lewy condition of the finest grid\n")
    f.write("CFL_NUMBER= 100.0\n")
    f.write("%\n")
    f.write("% Adaptive CFL number (NO, YES)\n")
    f.write("CFL_ADAPT= NO\n")
    f.write("%\n")
    f.write("% Parameters of the adaptive CFL number (factor down, factor up, CFL min value, CFL max value )\n")
    f.write("CFL_ADAPT_PARAM= ( 0.8, 1.2, 1.0, 1000.0)\n")
    f.write("%\n")
    f.write("%\n")

    f.write("% ------------------------ LINEAR SOLVER DEFINITION ---------------------------%\n")
    f.write("%\n")
    f.write("% Linear solver or smoother for implicit formulations (BCGSTAB, FGMRES, SMOOTHER)\n")
    f.write("LINEAR_SOLVER= FGMRES\n")
    f.write("%\n")
    f.write("% Preconditioner of the Krylov linear solver (ILU, LU_SGS, LINELET, JACOBI)\n")
    f.write("LINEAR_SOLVER_PREC= LU_SGS\n")
    f.write("%\n")
    f.write("% Min error of the linear solver for the implicit formulation\n")
    f.write("LINEAR_SOLVER_ERROR= 1E-4\n")
    f.write("%\n")
    f.write("% Max number of iterations of the linear solver for the implicit formulation\n")
    f.write("LINEAR_SOLVER_ITER= 5\n")
    f.write("%\n")
    f.write("%\n")




    f.write("% -------------------------- MULTIGRID PARAMETERS -----------------------------%\n")
    f.write("%\n")
    f.write("% ----------- NOT WORKING WITH PERIODIC BOUNDARY CONDITIONS !!!!! --------------%\n")
    f.write("%\n")
    f.write("%\n")




    f.write("% ----------------------- SLOPE LIMITER DEFINITION ----------------------------%\n")
    f.write("%\n")
    f.write("% Coefficient for the limiter\n")
    f.write("VENKAT_LIMITER_COEFF= 0.05\n")
    f.write("%\n")
    f.write("% Freeze the value of the limiter after a number of iterations\n")
    f.write("LIMITER_ITER= 999999\n")
    f.write("%\n")
    f.write("%\n")




    f.write("% -------------------- FLOW NUMERICAL METHOD DEFINITION -----------------------%\n")
    f.write("%\n")
    f.write("% Convective numerical method (JST, LAX-FRIEDRICH, CUSP, ROE, AUSM, HLLC,\n")
    f.write("%                              TURKEL_PREC, MSW)\n")
    f.write("CONV_NUM_METHOD_FLOW= LAX-FRIEDRICH\n")
    f.write("%\n")
    f.write("% Spatial numerical order integration (1ST_ORDER, 2ND_ORDER, 2ND_ORDER_LIMITER)\n")
    f.write("MUSCL_FLOW= NO\n")
    f.write("%\n")
    f.write("% Slope limiter (VENKATAKRISHNAN, VAN_ALBADA_EDGE)\n")
    f.write("SLOPE_LIMITER_FLOW= VENKATAKRISHNAN\n")
    f.write("%\n")
    f.write("% Entropy fix coefficient (0.0 implies no entropy fixing, 1.0 implies scalar artificial dissipation, 0.001 default)\n")
    f.write("ENTROPY_FIX_COEFF= 0.03\n")
    # f.write("ENTROPY_FIX_COEFF= 1.0\n")
    f.write("%\n")
    # f.write("% 2nd and 4th order artificial dissipation coefficients\n")
    # f.write("JST_SENSOR_COEFF= ( 0.5, 0.02 )\n")
    # f.write("JST_SENSOR_COEFF= ( 2, 0.2 )\n")
    f.write("%\n")
    f.write("% Time discretization (RUNGE-KUTTA_EXPLICIT, EULER_IMPLICIT, EULER_EXPLICIT)\n")
    f.write("TIME_DISCRE_FLOW= EULER_IMPLICIT\n")
    f.write("%\n")
    f.write("% Relaxation coefficient\n")
    f.write("%\n")
    f.write("%\n")




    f.write("% -------------------- TURBULENT NUMERICAL METHOD DEFINITION ------------------%\n")
    f.write("%\n")
    f.write("% Convective numerical method (SCALAR_UPWIND)\n")
    f.write("CONV_NUM_METHOD_TURB= SCALAR_UPWIND\n")
    f.write("%\n")
    f.write("% Monotonic Upwind Scheme for Conservation Laws (TVD) in the turbulence equations.\n")
    f.write("%           Required for 2nd order upwind schemes (NO, YES)\n")
    f.write("MUSCL_TURB= NO\n")
    f.write("%\n")
    f.write("% Slope limiter (VENKATAKRISHNAN, MINMOD)\n")
    f.write("SLOPE_LIMITER_TURB= VENKATAKRISHNAN\n")
    f.write("%\n")
    f.write("% Time discretization (EULER_IMPLICIT)\n")
    f.write("TIME_DISCRE_TURB= EULER_IMPLICIT\n")
    f.write("%\n")
    f.write("% Reduction factor of the CFL coefficient in the turbulence problem\n")
    f.write("CFL_REDUCTION_TURB= 1.0\n")
    f.write("%\n")
    f.write("%\n")




    f.write("% --------------------------- CONVERGENCE PARAMETERS --------------------------%\n")
    f.write("%\n")
    f.write("% Number of total iterations\n")
    f.write("ITER= 25000\n")      
    f.write("%\n")
    f.write("% Convergence criteria (CAUCHY, RESIDUAL)\n")
    f.write("CONV_CRITERIA= RESIDUAL\n")
    f.write("%\n")
    f.write("% Min value of the residual (log10 of the residual)\n")
    f.write("CONV_RESIDUAL_MINVAL= -8\n")
    f.write("%\n")
    f.write("% Start convergence criteria at iteration number\n")
    f.write("CONV_STARTITER= 10\n")
    f.write("%\n")
    f.write("% Number of elements to apply the criteria\n")
    f.write("CONV_CAUCHY_ELEMS= 100\n")
    f.write("%\n")
    f.write("% Epsilon to control the series convergence\n")
    f.write("CONV_CAUCHY_EPS= 1E-6\n")
    f.write("%\n")
    f.write("%\n")




    f.write("% ------------------------- INPUT/OUTPUT INFORMATION --------------------------%\n")
    f.write("%\n")
    f.write("SCREEN_OUTPUT= (INNER_ITER, RMS_DENSITY,RMS_MOMENTUM-X,RMS_MOMENTUM-Y,RMS_MOMENTUM-Z,RMS_ENERGY,WALL_TIME,MIN_CFL,MAX_CFL,AVG_CFL)\n")
    f.write("%\n")
    f.write("% Mesh input file\n")
    f.write("MESH_FILENAME= "+MESH_filename+"\n")
    f.write("%\n")
    f.write("% Mesh input file format (SU2, CGNS, NETCDF_ASCII)\n")
    f.write("MESH_FORMAT= CGNS\n")
    f.write("%\n")
    f.write("% Mesh output file\n")
    f.write("%MESH_OUT_FILENAME= periodic_mesh"+elements_id+".su2\n")
    f.write("%\n")
    f.write("% Restart flow input file\n")
    f.write("%SOLUTION_FILENAME= restart_BFM_"+elements_id+".dat\n")
    f.write("%\n")
    f.write("% Restart adjoint input file\n")
    f.write("SOLUTION_ADJ_FILENAME= solution_adj"+elements_id+".dat\n")
    f.write("%\n")
    f.write("% Output file format (PARAVIEW, TECPLOT, STL)\n")
    f.write("TABULAR_FORMAT= CSV\n")
    f.write("%\n")
    f.write("% Output file convergence history (w/o extension)\n")
    f.write("CONV_FILENAME= history"+elements_id+"\n")
    f.write("%\n")
    f.write("% Output file restart flow\n")
    f.write("RESTART_FILENAME= restart"+elements_id+".dat\n")
    f.write("%\n")
    f.write("% Output file restart adjoint\n")
    f.write("RESTART_ADJ_FILENAME= restart_adj"+elements_id+".dat\n")
    f.write("%\n")
    f.write("% Output file flow (w/o extension) variables\n")
    f.write("VOLUME_FILENAME= flow"+elements_id+"\n")
    f.write("%\n")
    f.write("VOLUME_OUTPUT = SOLUTION PRIMITIVE RESIDUAL\n")
    f.write("%\n")
    f.write("% Output file adjoint (w/o extension) variables\n")
    f.write("VOLUME_ADJ_FILENAME= adjoint"+elements_id+"\n")
    f.write("%\n")
    f.write("% Output objective function gradient (using continuous adjoint)\n")
    f.write("GRAD_OBJFUNC_FILENAME= of_grad"+elements_id+".dat\n")
    f.write("%\n")
    f.write("% Output file surface flow coefficient (w/o extension)\n")
    f.write("%SURFACE_FILENAME= surface_flow"+elements_id+"\n")
    f.write("%\n")
    f.write("% Output file surface adjoint coefficient (w/o extension)\n")
    f.write("SURFACE_ADJ_FILENAME= surface_adjoint"+elements_id+"\n")
    f.write("%\n")
    f.write("% Writing solution file frequency\n")
    f.write("OUTPUT_WRT_FREQ= 100\n")
    f.write("%\n")
    

    f.close()



def generate_SA_template(folder,file):
    file1 = open(file, 'r')
    Lines = file1.readlines()
    # for Line in Lines:
    #     var Line.split("=")[0]
    file1.close()

    file2 = open(folder+"\\Bladerow.template",'w')
    file2.write("# bladerow.template\n")
    file2.write("# One parameter per line. Whitespace otherwise ignored.\n")
    file2.write("# Comments start with # and continue to the end of the line\n")
    file2.write("# All parameters below are required)\n")

    for Line in Lines:
        var = Line.split("=")[0] 
        values = Line.split("=")[1].replace(" ","").replace("\n","").split(',')

        # # Parametrize stagger -------------------------------------------------------------------
        # if var == 'stagger':
        #     for i in range(len(values)):
        #         file2.write(var+str(i)+" = {stag"+str(i)+"="+str(values[i])+"}\n")
        # # #-----------------------------------------------------------------------------------------

        # # # Parametrize inlet metal angle -------------------------------------------------------------------
        # if var == 'theta_in':
        #     for i in range(len(values)):
        #         file2.write(var+str(i)+" = {the_in"+str(i)+"="+str(values[i])+"}\n")
        # # #-----------------------------------------------------------------------------------------

        # # # Parametrize outlet metal angle -------------------------------------------------------------------
        # if var == 'theta_out':
        #     for i in range(len(values)):
        #         file2.write(var+str(i)+" = {the_out"+str(i)+"="+str(values[i])+"}\n")
        # # #-----------------------------------------------------------------------------------------

        # # Parametrize inlet curvature radius -------------------------------------------------------------------
        if var == 'radius_in':
            for i in range(len(values)):
                file2.write(var+str(i)+" = {rad_in"+str(i)+"="+str(values[i])+"}\n")
        # #-----------------------------------------------------------------------------------------

        # # Parametrize outlet curvature radius -------------------------------------------------------------------
        if var == 'radius_out':
            for i in range(len(values)):
                file2.write(var+str(i)+" = {rad_out"+str(i)+"="+str(values[i])+"}\n")
        # #-----------------------------------------------------------------------------------------

        # # Parametrize inlet distance -------------------------------------------------------------------
        if var == 'dist_in':
            for i in range(len(values)):
                file2.write(var+str(i)+" = {dis_in"+str(i)+"="+str(values[i])+"}\n")
        # #-----------------------------------------------------------------------------------------

        # # Parametrize outlet distance  -------------------------------------------------------------------
        if var == 'dist_out':
            for i in range(len(values)):
                file2.write(var+str(i)+" = {dis_out"+str(i)+"="+str(values[i])+"}\n")
        # #-----------------------------------------------------------------------------------------

        #  # # Parametrize thickness  -------------------------------------------------------------------
        # if var == 'thickness_upper_1':
        #     for i in range(len(values)):
        #         file2.write(var+"_"+str(i)+" = {t_up_1_"+str(i)+"="+str(values[i])+"}\n")
        # # #-----------------------------------------------------------------------------------------
        #  # # Parametrize thickness  -------------------------------------------------------------------
        # if var == 'thickness_upper_2':
        #     for i in range(len(values)):
        #         file2.write(var+"_"+str(i)+" = {t_up_2_"+str(i)+"="+str(values[i])+"}\n")
        # # #-----------------------------------------------------------------------------------------
        #  # # Parametrize thickness  -------------------------------------------------------------------
        # if var == 'thickness_upper_3':
        #     for i in range(len(values)):
        #         file2.write(var+"_"+str(i)+" = {t_up_3_"+str(i)+"="+str(values[i])+"}\n")
        # # #-----------------------------------------------------------------------------------------
        #  # # Parametrize thickness  -------------------------------------------------------------------
        # if var == 'thickness_upper_4':
        #     for i in range(len(values)):
        #         file2.write(var+"_"+str(i)+" = {t_up_4_"+str(i)+"="+str(values[i])+"}\n")
        # # #-----------------------------------------------------------------------------------------
        #  # # Parametrize thickness  -------------------------------------------------------------------
        # if var == 'thickness_upper_5':
        #     for i in range(len(values)):
        #         file2.write(var+"_"+str(i)+" = {t_up_5_"+str(i)+"="+str(values[i])+"}\n")
        # # #-----------------------------------------------------------------------------------------
        #  # # Parametrize thickness  -------------------------------------------------------------------
        # if var == 'thickness_upper_6':
        #     for i in range(len(values)):
        #         file2.write(var+"_"+str(i)+" = {t_up_6_"+str(i)+"="+str(values[i])+"}\n")
        # # #-----------------------------------------------------------------------------------------

        #  # # Parametrize thickness  -------------------------------------------------------------------
        # if var == 'thickness_lower_1':
        #     for i in range(len(values)):
        #         file2.write(var+"_"+str(i)+" = {t_low_1_"+str(i)+"="+str(values[i])+"}\n")
        # # #-----------------------------------------------------------------------------------------
        #  # # Parametrize thickness  -------------------------------------------------------------------
        # if var == 'thickness_lower_2':
        #     for i in range(len(values)):
        #         file2.write(var+"_"+str(i)+" = {t_low_2_"+str(i)+"="+str(values[i])+"}\n")
        # # #-----------------------------------------------------------------------------------------
        #  # # Parametrize thickness  -------------------------------------------------------------------
        # if var == 'thickness_lower_3':
        #     for i in range(len(values)):
        #         file2.write(var+"_"+str(i)+" = {t_low_3_"+str(i)+"="+str(values[i])+"}\n")
        # # #-----------------------------------------------------------------------------------------
        #  # # Parametrize thickness  -------------------------------------------------------------------
        # if var == 'thickness_lower_4':
        #     for i in range(len(values)):
        #         file2.write(var+"_"+str(i)+" = {t_low_4_"+str(i)+"="+str(values[i])+"}\n")
        # # #-----------------------------------------------------------------------------------------
        #  # # Parametrize thickness  -------------------------------------------------------------------
        # if var == 'thickness_lower_5':
        #     for i in range(len(values)):
        #         file2.write(var+"_"+str(i)+" = {t_low_5_"+str(i)+"="+str(values[i])+"}\n")
        # # #-----------------------------------------------------------------------------------------
        #  # # Parametrize thickness  -------------------------------------------------------------------
        # if var == 'thickness_lower_6':
        #     for i in range(len(values)):
        #         file2.write(var+"_"+str(i)+" = {t_low_6_"+str(i)+"="+str(values[i])+"}\n")
        # # #-----------------------------------------------------------------------------------------



    file2.close()


def generate_SA_driver(folder,file):
    f = open(folder+"\\driver.ps1",'w')
    f.write("# Need: dprepro, python on path\n")
    f.write("\n")
    f.write("# For debugging\n")
    f.write("#Set-PSDebug -Trace 1\n")
    f.write("\n")
    f.write("# $1 and $2 are special variables in bash that contain the 1st and 2nd\n")
    f.write("# command line arguments to the script, which are the names of the\n")
    f.write("# Dakota parameters and results files, respectively.\n")
    f.write("\n")
    f.write("$params=$args[0]\n")
    f.write("$results=$args[1]\n")
    f.write("############################################################################### \n")
    f.write("##\n")
    f.write("## Pre-processing Phase -- Generate/configure an input file for your simulation \n")
    f.write("##  by substiting in parameter values from the Dakota parameters file.\n")
    f.write("##\n")
    f.write("###############################################################################\n")
    f.write("\n")
    f.write("$pyprepro_py = (get-command 'pyprepro').Path\n")
    f.write("python $pyprepro_py -I $params Bladerow.template Bladerow.i\n")
    f.write("\n")
    f.write("############################################################################### \n")
    f.write("##\n")
    f.write("## Execution Phase -- Run your simulation\n")
    f.write("##\n")
    f.write("###############################################################################\n")
    f.write("\n")
    f.write("python -m Parablade2BFM "+file+"\n")
    f.write("\n")
    f.write("###############################################################################\n")
    f.write("##\n")
    f.write("## Post-processing Phase -- Extract (or calculate) quantities of interest\n")
    f.write("##  from your simulation's output and write them to a properly-formatted\n")
    f.write("##  Dakota results file.\n")
    f.write("##\n")
    f.write("###############################################################################\n")
    f.write("\n")
    f.write("$eff         = (type Bladerow.log | select -First 1).Split('=')[1].Trim()\n")
    f.write('echo "$eff eff" | out-file -encoding ASCII $results\n')
    f.close()



def generate_SA_input(folder):
    file1 = open(folder+"\\Bladerow.template", 'r')
    Lines = file1.readlines()
    file1.close()
    
    var = []
    values = []
    for i in range(4,len(Lines)):
        var.append((Lines[i].split("{")[1]).split("=")[0])
        values.append(Lines[i].split("=")[2].replace("}\n",""))
    

    f = open(folder+"\\dakota_BFM_sensitivity.in",'w')
    f.write("environment\n")
    f.write("  tabular_data\n")
    f.write("    tabular_data_file = 'BFM_tabular.dat'\n")
    f.write("\n")
    f.write("\n")
    f.write("method\n")
    f.write("  centered_parameter_study\n")

    # f.write("    steps_per_variable = 0\n")
    f.write("    steps_per_variable = 1\n")

    f.write("    step_vector")
    for i in range(len(var)):
        f.write(" 1")
    f.write("\n")
    f.write("\n")
    f.write("\n")
    f.write("variables\n")
    f.write("  continuous_design = "+str(len(var))+"\n")
    f.write("    initial_point =")
    for i in range(len(var)):
        f.write(" "+str(values[i]))
    f.write("\n")

    f.write("    lower_bounds =")
    for i in range(len(var)):
        f.write(" "+str(values[i]))
    f.write("\n")

    f.write("    descriptors    ")
    for i in range(len(var)):
        f.write(" '"+str(var[i])+"'")
    f.write("\n")
    f.write("\n")
    f.write("interface\n")
    f.write("  analysis_drivers = 'powershell.exe -ExecutionPolicy Bypass -File driver.ps1' # For Windows\n")
    f.write("    fork\n")
    f.write("       aprepro # For Windows\n")
    f.write("\n")
    f.write("\n")
    f.write("responses\n")
    f.write("  response_functions = 1\n")
    f.write("  descriptors  'eff' \n")
    f.write(" no_gradients\n")
    # f.write("  numerical_gradients\n")
    # f.write("forward\n")
    # f.write("fd_gradient_step_size=1\n")
    f.write("  no_hessians\n")
    
    f.close()





def generate_OPT_template(folder,file):
    file1 = open(file, 'r')
    Lines = file1.readlines()
    # for Line in Lines:
    #     var Line.split("=")[0]
    file1.close()

    file2 = open(folder+"\\Bladerow.template",'w')
    file2.write("# bladerow.template\n")
    file2.write("# One parameter per line. Whitespace otherwise ignored.\n")
    file2.write("# Comments start with # and continue to the end of the line\n")
    file2.write("# All parameters below are required)\n")

    for Line in Lines:
        var = Line.split("=")[0] 
        values = Line.split("=")[1].replace(" ","").replace("\n","").split(',')

        # # Parametrize stagger -------------------------------------------------------------------
        # if var == 'stagger':
        #     for i in range(len(values)):
        #         file2.write(var+str(i)+" = {stag"+str(i)+"="+str(values[i])+"}\n")
        # # #-----------------------------------------------------------------------------------------

        # # # Parametrize inlet metal angle -------------------------------------------------------------------
        # if var == 'theta_in':
        #     for i in range(len(values)):
        #         file2.write(var+str(i)+" = {the_in"+str(i)+"="+str(values[i])+"}\n")
        # # #-----------------------------------------------------------------------------------------

        # # # Parametrize outlet metal angle -------------------------------------------------------------------
        # if var == 'theta_out':
        #     for i in range(len(values)):
        #         file2.write(var+str(i)+" = {the_out"+str(i)+"="+str(values[i])+"}\n")
        # # #-----------------------------------------------------------------------------------------

        # # Parametrize inlet curvature radius -------------------------------------------------------------------
        if var == 'radius_in':
            for i in range(len(values)):
                file2.write(var+str(i)+" = {rad_in"+str(i)+"="+str(values[i])+"}\n")
        # #-----------------------------------------------------------------------------------------

        # # Parametrize outlet curvature radius -------------------------------------------------------------------
        if var == 'radius_out':
            for i in range(len(values)):
                file2.write(var+str(i)+" = {rad_out"+str(i)+"="+str(values[i])+"}\n")
        # #-----------------------------------------------------------------------------------------

        # # Parametrize inlet distance -------------------------------------------------------------------
        if var == 'dist_in':
            file2.write(var+"2 = {dis_in2="+str(values[2])+"}\n")
        #     for i in range(len(values)):
        #         file2.write(var+str(i)+" = {dis_in"+str(i)+"="+str(values[i])+"}\n")
        # # #-----------------------------------------------------------------------------------------

        # # Parametrize outlet distance  -------------------------------------------------------------------
        if var == 'dist_out':
            for i in range(2,4):
            # for i in range(len(values)):
                file2.write(var+str(i)+" = {dis_out"+str(i)+"="+str(values[i])+"}\n")
        # # #-----------------------------------------------------------------------------------------

        #  # # Parametrize thickness  -------------------------------------------------------------------
        # if var == 'thickness_upper_1':
        #     for i in range(len(values)):
        #         file2.write(var+"_"+str(i)+" = {t_up_1_"+str(i)+"="+str(values[i])+"}\n")
        # # #-----------------------------------------------------------------------------------------
        #  # # Parametrize thickness  -------------------------------------------------------------------
        # if var == 'thickness_upper_2':
        #     for i in range(len(values)):
        #         file2.write(var+"_"+str(i)+" = {t_up_2_"+str(i)+"="+str(values[i])+"}\n")
        # # #-----------------------------------------------------------------------------------------
         # # Parametrize thickness  -------------------------------------------------------------------
        if var == 'thickness_upper_3':
            file2.write(var+"_0 = {t_up_3_0="+str(values[0])+"}\n")
        #     for i in range(len(values)):
        #         file2.write(var+"_"+str(i)+" = {t_up_3_"+str(i)+"="+str(values[i])+"}\n")
        # # #-----------------------------------------------------------------------------------------
         # # Parametrize thickness  -------------------------------------------------------------------
        if var == 'thickness_upper_4':
            file2.write(var+"_0 = {t_up_4_0="+str(values[0])+"}\n")
            # for i in range(len(values)):
            #     file2.write(var+"_"+str(i)+" = {t_up_4_"+str(i)+"="+str(values[i])+"}\n")
        # #-----------------------------------------------------------------------------------------
        #  # # Parametrize thickness  -------------------------------------------------------------------
        # if var == 'thickness_upper_5':
        #     for i in range(len(values)):
        #         file2.write(var+"_"+str(i)+" = {t_up_5_"+str(i)+"="+str(values[i])+"}\n")
        # # #-----------------------------------------------------------------------------------------
        #  # # Parametrize thickness  -------------------------------------------------------------------
        # if var == 'thickness_upper_6':
        #     for i in range(len(values)):
        #         file2.write(var+"_"+str(i)+" = {t_up_6_"+str(i)+"="+str(values[i])+"}\n")
        # # #-----------------------------------------------------------------------------------------

        #  # # Parametrize thickness  -------------------------------------------------------------------
        # if var == 'thickness_lower_1':
        #     for i in range(len(values)):
        #         file2.write(var+"_"+str(i)+" = {t_low_1_"+str(i)+"="+str(values[i])+"}\n")
        # # #-----------------------------------------------------------------------------------------
        #  # # Parametrize thickness  -------------------------------------------------------------------
        # if var == 'thickness_lower_2':
        #     for i in range(len(values)):
        #         file2.write(var+"_"+str(i)+" = {t_low_2_"+str(i)+"="+str(values[i])+"}\n")
        # # #-----------------------------------------------------------------------------------------
         # # Parametrize thickness  -------------------------------------------------------------------
        if var == 'thickness_lower_3':
            file2.write(var+"_0 = {t_low_3_0="+str(values[0])+"}\n")
        #     for i in range(len(values)):
        #         file2.write(var+"_"+str(i)+" = {t_low_3_"+str(i)+"="+str(values[i])+"}\n")
        # # #-----------------------------------------------------------------------------------------
         # # Parametrize thickness  -------------------------------------------------------------------
        if var == 'thickness_lower_4':
            file2.write(var+"_0 = {t_low_4_0="+str(values[0])+"}\n")
        #     for i in range(len(values)):
        #         file2.write(var+"_"+str(i)+" = {t_low_4_"+str(i)+"="+str(values[i])+"}\n")
        # # #-----------------------------------------------------------------------------------------
        #  # # Parametrize thickness  -------------------------------------------------------------------
        # if var == 'thickness_lower_5':
        #     for i in range(len(values)):
        #         file2.write(var+"_"+str(i)+" = {t_low_5_"+str(i)+"="+str(values[i])+"}\n")
        # # #-----------------------------------------------------------------------------------------
        #  # # Parametrize thickness  -------------------------------------------------------------------
        # if var == 'thickness_lower_6':
        #     for i in range(len(values)):
        #         file2.write(var+"_"+str(i)+" = {t_low_6_"+str(i)+"="+str(values[i])+"}\n")
        # # #-----------------------------------------------------------------------------------------



    file2.close()


def generate_OPT_driver(folder,file):
    f = open(folder+"\\driver.ps1",'w')
    f.write("# Need: dprepro, python on path\n")
    f.write("\n")
    f.write("# For debugging\n")
    f.write("#Set-PSDebug -Trace 1\n")
    f.write("\n")
    f.write("# $1 and $2 are special variables in bash that contain the 1st and 2nd\n")
    f.write("# command line arguments to the script, which are the names of the\n")
    f.write("# Dakota parameters and results files, respectively.\n")
    f.write("\n")
    f.write("$params=$args[0]\n")
    f.write("$results=$args[1]\n")
    f.write("############################################################################### \n")
    f.write("##\n")
    f.write("## Pre-processing Phase -- Generate/configure an input file for your simulation \n")
    f.write("##  by substiting in parameter values from the Dakota parameters file.\n")
    f.write("##\n")
    f.write("###############################################################################\n")
    f.write("\n")
    f.write("$pyprepro_py = (get-command 'pyprepro').Path\n")
    f.write("python $pyprepro_py -I $params Bladerow.template Bladerow.i\n")
    f.write("\n")
    f.write("############################################################################### \n")
    f.write("##\n")
    f.write("## Execution Phase -- Run your simulation\n")
    f.write("##\n")
    f.write("###############################################################################\n")
    f.write("\n")
    f.write("python -m Parablade2BFM "+file+"\n")
    f.write("\n")
    f.write("###############################################################################\n")
    f.write("##\n")
    f.write("## Post-processing Phase -- Extract (or calculate) quantities of interest\n")
    f.write("##  from your simulation's output and write them to a properly-formatted\n")
    f.write("##  Dakota results file.\n")
    f.write("##\n")
    f.write("###############################################################################\n")
    f.write("\n")
    f.write("$eff         = (type Bladerow.log | select -First 1).Split('=')[1].Trim()\n")
    f.write('echo "$eff eff" | out-file -encoding ASCII $results\n')
    f.close()



def generate_OPT_input(folder):
    file1 = open(folder+"\\Bladerow.template", 'r')
    Lines = file1.readlines()
    file1.close()
    
    var = []
    values = []
    for i in range(4,len(Lines)):
        var.append((Lines[i].split("{")[1]).split("=")[0])
        values.append(Lines[i].split("=")[2].replace("}\n",""))
    

    f = open(folder+"\\dakota_BFM_optimization.in",'w')
    f.write("environment\n")
    f.write("  tabular_data\n")
    f.write("    tabular_data_file = 'BFM_tabular.dat'\n")
    f.write("\n")
    f.write("\n")
    f.write("method\n")
    f.write("  conmin_frcg\n")
    f.write("    convergence_tolerance 1.0e-2\n")
    f.write("    max_function_evaluations 200")
    for i in range(len(var)):
        f.write(" 1")
    f.write("\n")
    f.write("\n")
    f.write("\n")
    f.write("variables\n")
    f.write("  continuous_design = "+str(len(var))+"\n")
    f.write("    initial_point ")
    for i in range(len(var)):
        f.write(" "+str(values[i]))
    f.write("\n")
    f.write("    descriptors    ")
    for i in range(len(var)):
        f.write(" '"+str(var[i])+"'")
    f.write("\n")
    f.write("    upper_bounds")
    for i in range(len(var)):
        if "dis" in var[i]:
            f.write(" 1 " )
        elif "rad" in var[i]:
            f.write(" 0.01 " )
        else:
            f.write(" 0.1 " )
            
    f.write("\n")
    f.write("    lower_bounds")
    for i in range(len(var)):
        if "dis" in var[i]:
            f.write(" 0 " )
        else:
            f.write(" 0.0001 " )
            
    f.write("\n")



    f.write("\n")
    f.write("\n")
    f.write("interface\n")
    f.write("  analysis_drivers = 'powershell.exe -ExecutionPolicy Bypass -File driver.ps1' # For Windows\n")
    f.write("    fork\n")
    f.write("       aprepro # For Windows\n")
    f.write("\n")
    f.write("\n")
    f.write("responses\n")
    f.write("  objective_functions 1\n")
    f.write("  descriptors  'eff' \n")
    f.write("  sense  'max' \n")
    f.write("  numerical_gradients\n")
    f.write("forward\n")
    f.write("  no_hessians\n")
    
    f.close()



def ReadUserInput(name):
    IN = {}
    infile = open(name, 'r')
    for line in infile:
      words = re.split('=| |\n|,|[|]', line)
      if not any(words[0] in s for s in ['\n', '%', ' ', '#']):
        words = list(filter(None, words))
        for i in range(0, len(words)):
            try:
                words[i] = float(words[i])
            except:
                words[i] = words[i]
        if len(words[1::1]) == 1 and isinstance(words[1], float):
            IN[words[0]] = [words[1]]
        elif len(words[1::1]) == 1 and isinstance(words[1], str):
            IN[words[0]] = words[1]
        else:
            IN[words[0]] = words[1::1]
    IN['Config_Path'] = name
    return IN