import numpy as np
import math
import sys
import os
from subprocess import Popen, PIPE

from FILE_CREATION import *


def EXISTING_GEOMETRY(mesh,settings):
    
     generate_P2BFM_input(settings,mesh)

     # Meangen2BFM.py /home/gabbo/Documents/bitbucket_local/fan-stage-design/M2P.cfg
     P2BFM = os.environ["P2BFM"]
     os.chdir(P2BFM)
     file="P2BFM_"+str(settings.MACHINE_NAME)+".cfg"
     os.system("python -m Parablade2BFM "+file)
     generate_BFM_cfg(P2BFM,settings)



     return ("Geometry has been designed!")

