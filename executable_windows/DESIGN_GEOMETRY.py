import numpy as np
import math
import sys
import os
from subprocess import Popen, PIPE
import time
import shutil

from design_steps import *
from BLADE_CLASS import blade_shape
from FILE_CREATION import *
# from SETTINGS_CLASS import *


def DESIGN_GEOMETRY(mesh,settings):
    

     # 2) Compute ideal work
     L_is = ideal_work(settings)         # [J]
     Pt3= settings.Pt_in* settings.beta_tt

     # if settings.N_ROWS == 2:
     if 2>1:   #cavolata per togliere sopra
          # j=0
          err_eta_TT_stage=1
          while err_eta_TT_stage>1e-5:
          # errPt=1
          # while errPt>1e-5:
               
               # 3) Compute real work
               L_r = real_work(settings,L_is)                    # [J]
     
               # 4),5) Find velocity triangles at location 1, find thermodinamic state
               state1 = rotor_inlet(settings,L_is,L_r)
               # settings.psi = L_is/state1.U**2
               print (" Psi from L_is and U is", L_is/state1.U**2)
     
               # 6) Find blade height
               settings.b = blade_height (settings,state1)
               W_tip = np.sqrt(state1.Wa**2 + (state1.Vt-state1.U*(settings.Rm+settings.b/2)/settings.Rm)**2)
               state1.M_r_tip =  W_tip/np.sqrt(settings.gamma*settings.R*state1.T)
               settings.RPM = state1.U/ settings.Rm * 9.5493
     
     
               err_eta_TT_r =1
               i=0
               settings.eta_TT_r = settings.eta_TT_stage
               while err_eta_TT_r >1e-5:
                    # 7) Find velocity triangles in section 2 and 8) find thermodynamic point. 
                    # Assumptions: - Constant blade height b1 = b2 
                    #              - Constant axial velocity Va1 = Va2 ---> 9) Inner iteration for mass continuity drops this hp. 
                    state2 , settings.ki = rotor_outlet(settings,L_r,state1)      # I am setting the newly found ki
          
                    eta_TT_r_nolosses = (state2.Tt_is-state1.Tt)/(state2.Tt-state1.Tt)
                    # 10), 11) and 12) skipped because I am not interested in a radial span design
                    # 13) Insert losses
                    state2_losses = rotor_losses(settings,state1,state2)
                    #13.1) Endwall losses (annulus + secondary) - Howell model
                    #13.2) Tip clearance losses - Lakshiminarayana
                    #13.3) Profile losses - Lieblein
          
                    # 14) Iterate over eta_r in order to have a new efficiency coherent with the losses  
                    eta_TT_r_new, state2 = eta_r_losses(settings,state1,state2,state2_losses)
                    err_eta_TT_r = abs(eta_TT_r_new-settings.eta_TT_r)
                    i+=1
                    settings.eta_TT_r = eta_TT_r_new
     
               # 15)  Find velocity triangles and 16) find thermodynamic point in section 3 (stator outlet)
               state3 = stator_outlet(settings,state1,state2)
     
               # 17) Insert stator losses
               state3_losses = stator_losses(settings,state3,state2)
     
               # 18) Iterate the whole design over eta_tt stage

               # #-------------------------------------------------------------
               # errPt = Pt3 - state3.Pt
               # Pt3_con_perdite = state3.Pt
               # Pt3_is =  state2.Pt2_is
               # Tt_out_s = state1.Tt * (Pt3/state1.Pt) ** ((settings.gamma - 1)/settings.gamma)
               # eta_TT_stage_new = (Tt_out_s - state1.Tt) / (state3.Tt - state1.Tt)
               # # eta_TT_stage_new = 
               #--------------------------------------------------------------

               eta_TT_stage_new, state3 = eta_tt_losses(settings,state1,state2,state3,state3_losses)
               err_eta_TT_stage = abs(eta_TT_stage_new-settings.eta_TT_stage)

               errPt = Pt3 - state3.Pt
               Pt3_con_perdite = state3.Pt
               Pt3_is =  state2.Pt2_is
               # j+=1
               settings.eta_TT_stage = eta_TT_stage_new











          # Now I can create the blade shape using Lieblein correlation for optimal loading and compute B.L. parameters

          Blade1 = blade_shape("rotor","lieblein",settings,state1,state2)
          Blade2 = blade_shape("stator","lieblein",settings,state2,state3)

          print ('Blades at midspan has been designed!')

          generate_M2P_input_2BLADES(settings,mesh,state1,state3,state2_losses,state3_losses,Blade1,Blade2)



          # I have to get the ouput static pressure for P2BFM. I take the pressure after the rotor since I want to represent only that
          settings.P_out = state2.P
      

          m_dot_corrected = settings.m_dot*np.sqrt(state2.Tt/288.15)/(state2.Pt/101325) 
          m_dot_corrected2 = settings.m_dot*np.sqrt(state3.Tt/288.15)/(state3.Pt/101325) 
          coor = np.sqrt(state2.Tt/288.15)/(state2.Pt/101325)
          m_out = m_dot_corrected * (26500/101325)/np.sqrt(244/288.15)
          beta_effettivo = state2.Pt / state1.Pt
          
          # Parameter design--------------------
          settings.T_in = state1.T
          settings.P_in = state1.P
          settings.rho_in = state1.rho
          settings.M_in = state1.M
          print("omega is ", state1.U/settings.Rm)
          print("And RPM are", state1.U/settings.Rm*9.5493)
          settings.RPM = state1.U/settings.Rm*9.5493
          settings.V_in = state1.V

          

     #      # Meangen2BFM.py /home/gabbo/Documents/bitbucket_local/fan-stage-design/M2P.cfg
     #      M2BFM = os.environ["M2BFM"]
     #      os.chdir(M2BFM)
     #      os.system("python -m Meangen2BFM M2P_fan_stage.cfg")
     #      settings.Pt_in = settings.Pt1 
     #      settings.Tt_in = settings.Tt
     #      settings.T_in = state1.T
     #      settings.P_in = state1.P 
     #      settings.rho_in = state1.rho
     #      settings.M_in = state1.M
     #      settings.Va_in = state1.Va
     #      settings.P_out  = state3.P
     #      settings.RPM = state1.U /settings.Rm* 60/2/np.pi
     #      generate_BFM_cfg(M2BFM,settings)
     
     # elif settings.N_ROWS==1:

     #      err_eta_TT_r=1
     #      i=0
     #      while err_eta_TT_r>1e-5:
     
     #           # 3) Compute real work
     #           L_r = real_work(settings,L_is)                    # [J]
     
     #           # 4),5) Find velocity triangles at location 1, find thermodinamic state
     #           state1 = rotor_inlet(settings,L_r)
     
     #           # 6) Find blade height
     #           settings.b = blade_height (settings,state1)
     #           RPM = state1.U/ settings.Rm * 9.5493

     #           # 7) Find velocity triangles in section 2 and 8) find thermodynamic point. 
     #           # Assumptions: - Constant blade height b1 = b2 
     #           #              - Constant axial velocity Va1 = Va2 ---> 9) Inner iteration for mass continuity drops this hp. 
     #           state2 , settings.ki = rotor_outlet(settings,L_r,state1)      # I am setting the newly found ki
          
          
     #           # 10), 11) and 12) skipped because I am not interested in a radial span design
     #           # 13) Insert losses
     #           state2_losses = rotor_losses(settings,state1,state2)
     #           #13.1) Endwall losses (annulus + secondary) - Howell model
     #           #13.2) Tip clearance losses - Lakshiminarayana
     #           #13.3) Profile losses - Lieblein
          
     #           # 14) Iterate over eta_r in order to have a new efficiency coherent with the losses  
     #           eta_TT_r_new, state2 = eta_r_losses(settings,state1,state2,state2_losses)
     #           err_eta_TT_r = abs(eta_TT_r_new-settings.eta_TT_r)
     #           i+=1
     #           settings.eta_TT_r = eta_TT_r_new

     #      # Now I can create the blade shape using Lieblein correlation for optimal loading and compute B.L. parameters
     #      Blade1 = blade_shape("rotor","lieblein",settings,state1,state2)

     #      print ('Blade at midspan has been designed!')

          
     #      generate_Meangen_input_1BLADE()


     #      generate_M2P_input1BLADE(settings,mesh,state1,state2,Blade1)

      

   


     # HOME = os.getenv('M2BFM')
     # sys.path.append(HOME + "\\executables_windows")

     from Meangen2Parablade import Meangen2Parablade


     # Reading input file
     DIR = os.getcwd()
     # try:
     #     INFile = DIR + sys.argv[-1]
     # except:
     #     INFile = DIR + 'M2P.cfg'      # Default File name
     # try:
     #     IN = ReadUserInput(INFile)
     # except:
     #     raise Exception('\n\n\n''Something went wrong when reading the configuration file,exiting the program...'
     #                     '\n\nTo call MakeBlade.py from terminal type:'
     #                     '\n\tMakeBlade.py <configuration file name>')
     t_start = time.time()

     # Manual input
     INFile = DIR + '\\templates\\M2P_MYfan.cfg'

     IN = ReadUserInput(INFile)

     # Executing Meangen and writing Parablade input files.
     M = Meangen2Parablade(IN)


     if os.path.isdir(str(settings.MACHINE_NAME)):
          shutil.rmtree(DIR+"\\"+str(settings.MACHINE_NAME))
     os.mkdir(DIR + "\\"+settings.MACHINE_NAME)

        # Looping over the number of stages to create folders for each stage and respective bladerow.
     MACHINEDIR = DIR + "\\"+str(settings.MACHINE_NAME)
     for i in range(settings.N_STAGE):
          if os.path.isdir(MACHINEDIR+"\\Stage_"+str(i+1)):
               for j in range(1,settings.N_ROWS+1):
                    if os.path.isdir(MACHINEDIR+"\\Stage_"+str(i+1) + "\\Bladerow_"+str(j)):
                        shutil.move(DIR + "\\templates\\Bladerow_"+str(2*i + 1) + ".cfg", MACHINEDIR+"\\Stage_"+str(i+1)+"\\Bladerow_"+str(j)+"\\Bladerow.cfg")
                    else:
                        os.mkdir(MACHINEDIR+"\\Stage_"+str(i+1)+"\\Bladerow_"+str(j))
                        shutil.move(DIR + "\\templates\\Bladerow_" + str(2 * i + 1) + ".cfg", MACHINEDIR + "\\Stage_" + str(i + 1) + "\\Bladerow_"+str(j)+"\\Bladerow.cfg")
            
                    # if os.path.isdir(MACHINEDIR+"\\Stage_"+str(i+1) + "\\Bladerow_"+str(j)+"_BFM"):
                    #     shutil.move(DIR + "\\templates\\Bladerow_"+str(2*i + 1) + "_BFM.cfg", MACHINEDIR+"\\Stage_"+str(i+1)+"\\Bladerow_"+str(j)+"_BFM\\Bladerow.cfg")
                    # else:
                    #     os.mkdir(MACHINEDIR+"\\Stage_"+str(i+1)+"\\Bladerow_1_BFM")
                    #     shutil.move(DIR + "\\templates\\Bladerow_" + str(2 * i + 1) + "_BFM.cfg", MACHINEDIR + "\\Stage_" + str(i + 1) + "\\Bladerow_"+str(j)+"_BFM\\Bladerow.cfg")


          else:
                os.mkdir(MACHINEDIR+"\\Stage_"+str(i+1))
                for j in range(1,settings.N_ROWS+1):
                    os.mkdir(MACHINEDIR+"\\Stage_" + str(i + 1) + "\\Bladerow_"+str(j))
                    shutil.move(DIR + "\\templates\\Bladerow_" + str(j) + ".cfg", MACHINEDIR + "\\Stage_" + str(i + 1) + "\\Bladerow_"+str(j)+"\\Bladerow.cfg")
  
                    # os.mkdir(MACHINEDIR+"\\Stage_" + str(i + 1) + "\\Bladerow_"+str(j)+"_BFM")
                    # shutil.move(DIR + "\\templates\\Bladerow_" + str(j) + "_BFM.cfg", MACHINEDIR + "\\Stage_" + str(i + 1) + "\\Bladerow_"+str(j)+"_BFM\\Bladerow.cfg")

          PARA_testcases = os.getenv('P2BFM') + "\\testcases"

          if os.path.isdir(PARA_testcases+"\\"+str(settings.MACHINE_NAME)):
               shutil.rmtree(PARA_testcases+"\\"+str(settings.MACHINE_NAME))
          os.mkdir(PARA_testcases+"\\"+str(settings.MACHINE_NAME))
          for i in range(1,settings.N_STAGE+1):
               os.mkdir(PARA_testcases+"\\"+str(settings.MACHINE_NAME)+"\\Stage_"+str(i))
               for j in range(1,settings.N_ROWS+1):
                    os.mkdir(PARA_testcases+"\\"+str(settings.MACHINE_NAME)+"\\Stage_"+str(i)+"\\Bladerow_"+str(j))
                    shutil.copy(MACHINEDIR+"\\Stage_"+str(i)+"\\Bladerow_"+str(j)+"\\Bladerow.cfg",PARA_testcases+"\\"+str(settings.MACHINE_NAME)+"\\Stage_"+str(i)+"\\Bladerow_"+str(j)+"\\Bladerow.cfg")
    
 
     # settings.RPM = state1.U/settings.Rm*9.5493

     return (settings)

