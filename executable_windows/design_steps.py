
import math
import numpy as np
from State_class import State
from compressor_losses import Losses
# from testcases.settings_R67 import Cp
# from templates.settings_myfan import Rm



def ideal_work (settings):

    # Inport variables -----------------------------------------------------------------------#
    Cp = settings.Cp
    Tt_in = settings.Tt_in
    #T_in = settings.T_in
    beta_tt = settings.beta_tt
    beta_r = settings.beta_r
    gamma = settings.gamma
    #-----------------------------------------------------------------------------------------#
    
    # if settings.N_ROWS == 2:
    L_is =  Cp*Tt_in*(beta_tt**((gamma-1)/gamma)-1)          # [J]
    # elif settings.N_ROWS == 1:
    #     L_is = Cp*T_in*(beta_r**((gamma-1)/gamma)-1)          # [J]
    return L_is




def real_work(settings,L_is):

    # Inport variables -----------------------------------------------------------------------#
    eta_TT_stage = settings.eta_TT_stage
    eta_TT_r = settings.eta_TT_r
   
    #-----------------------------------------------------------------------------------------#
    
    # L_r = L_is/eta_TT_stage                # [J]
    L_r = L_is/eta_TT_r                # [J]

    # if settings.N_ROWS == 2:
    #      L_r = L_is/eta_TT_stage                # [J]
    # elif settings.N_ROWS == 1:
    #      L_r = L_is/eta_TT_r  
    return L_r
    



def rotor_inlet(settings,L_is,L_eul):

    # Inport variables -----------------------------------------------------------------------#
    psi = settings.psi
    phi = settings.phi
    # ki = settings.ki
    Tt1 = settings.Tt_in
    Pt1 = settings.Pt_in
    Cp = settings.Cp
    gamma = settings.gamma
    R = settings.R
    alpha1 = settings.alpha1
    #-----------------------------------------------------------------------------------------#


    # 4),5) Find velocity triangles at location 1, find thermodinamic state 
    # U_m = np.sqrt(L_is/psi)
    U_m = np.sqrt(L_eul/psi)

    print("Um from Lis and psi is", U_m)
    # U_m = settings.V_in/settings.phi
    



    # Va1_m = settings.V_in
    # settings.phi = Va1_m/U_m
    Va1_m = U_m*phi
    print("Va from U and phi is", Va1_m)
    Vt1_m = Va1_m * math.tan(math.radians(alpha1))
    
    state1 = State()
    state1.vel_triangles(Va1_m,Vt1_m,U_m)
    state1.thermodynamic_1(Tt1,Pt1,Cp,gamma,R)
    
    # state1.B_L(settings)
    
    return state1





# # 6) Find blade height from mass continuity at location 1
def blade_height (settings,state1):
    
     # Inport variables -----------------------------------------------------------------------#
     m_dot = settings.m_dot
     Rm = settings.Rm
     Va = state1.Va
     rho = state1.rho
     chord = settings.c
     thick = settings.t_max  # Percentage of the chord
     N = settings.N          # Number of blades
       #-----------------------------------------------------------------------------------------#
    
    # Insert Area correction due to blade metal blockage of the channel 
     alpha = np.arcsin(chord*thick/2/Rm) * 180/np.pi
     corr = N*alpha/360

    #  b = m_dot/(2*np.pi*Rm*Va*rho)                     # No blade correction
     b =  m_dot/(2*np.pi*Rm*Va*rho)/(1-corr)
     
     return b




def rotor_outlet(settings,L_r,state1):

    # Inport variables -----------------------------------------------------------------------#
    eta_TT_r = settings.eta_TT_r
    Cp = settings.Cp
    ki = settings.ki
    gamma = settings.gamma
    R  = settings.R
    #-----------------------------------------------------------------------------------------#

 #7) Find velocity triangles in section 2
    # Assumptions: - Constant blade height b1 = b2 
    #              - Constant axial velocity Va1 = Va2
    Va1_m = state1.Va
    Vt1_m = state1.Vt
    U_m = state1.U

    Vt2_m = L_r/U_m + Vt1_m
    Va2_m = Va1_m
    # Va2_m_old=Va2_m
    state2 = State()
    err = 1
    i=0
    # ki_new=ki
    while err > 1e-5:                  # 9) Iterate over Va2_m i order to mantain continuity eq. for fixed b
        state2.vel_triangles(Va2_m,Vt2_m,U_m)

        # 8) Find thermodynamic state in section 2
        state2.thermodynamic_2(state1,L_r,eta_TT_r,Cp,gamma,R)
        # state2.B_L(settings)
        Va2_m_new = (state1.rho * state1.Va)/state2.rho
        err = abs(Va2_m_new-Va2_m)
        Va2_m=Va2_m_new
        i += 1

        ki_new = (state1.Va**2 - state2.Va**2 + state1.Vt**2 - state2.Vt**2 +2*U_m*(state2.Vt-state1.Vt))/(2*L_r)
        # T2= ki_new*L_r/Cp+state1.T
        # T2_new = ki_new*L_r/Cp+state1.T


    return (state2, ki_new)




def rotor_losses(settings,state1,state2):
    # 13) Insert losses
    state2_losses=Losses()
    #13.1) Endwall losses (annulus + secondary) - Howell model
    state2_losses.endwall_losses(settings,state1,state2)
    #13.2) Tip clearance losses - Lakshiminarayana
    state2_losses.tip_losses(settings,state1,state2)
    #13.3) Profile losses - Lieblein
    state2_losses.profile_losses('rotor',settings, state1,state2)

    return state2_losses



def eta_r_losses(settings,state1,state2,state2_losses):

     # Inport variables -----------------------------------------------------------------------#
    gamma = settings.gamma
    Y = state2_losses.Y
    delta_eta = state2_losses.delta_eta
    Pt1_r = state1.Pt_r
    T1 = state1.T
    T2 = state2.T
    P1 = state1.P
    Pt1 = state1.Pt
    Tt1= state1.Tt
    Tt2 = state2.Tt
    #-----------------------------------------------------------------------------------------#

    
    Pt2_r = Pt1_r - (Pt1_r - P1)*Y
    P2corretta = Pt2_r*(1+(gamma-1)/2*state2.M_r**2)**(-gamma/(gamma-1))
    P2 = Pt2_r - 0.5*state2.rho*state2.W**2              # From Pt2_r i can find P2. Use it to find eta_r (see Baksharone pg. 235)
    Pt2 = P2 + 0.5*state2.rho*state2.V**2                # N.B. BETTER TO PROCEED WITH SIMPLE SURE WAY 

    state2.Pt2_is = (Pt1_r - 0.5*state2.rho*state2.W**2)+ 0.5*state2.rho*state2.V**2 
    P2_is =   state1.Pt * (state2.T_is/state1.Tt)**(gamma/(gamma-1))  

    # eta_r_new = ((P2/P1)**((gamma-1)/gamma)-1)/(T2/T1 -1)
    Tt1= state1.Tt
    Tt2 = state2.Tt

    Tt2_r_is = state1.Tt_r*(P2/Pt2_r)**((gamma-1)/gamma)
    Tt2_is_corretta = Tt2_r_is -0.5*(state2.W**2)/settings.Cp +0.5*(state2.V**2)/settings.Cp
    eta_TT_r_new_corretta = (Tt2_is_corretta-Tt1)/(Tt2-Tt1)
    
    Tt2_is = Tt1*(Pt2/Pt1)**((gamma-1)/gamma)
    T2_is = T1*(P2/P1)**((gamma-1)/gamma)
    eta_TS_r_new = (T2_is-Tt1)/(T2-Tt1)
    eta_TT_r_new = (Tt2_is-Tt1)/(Tt2-Tt1)
    eta_r_new = eta_TT_r_new - delta_eta
    
    
    
    # # Pt1_r = state1.Pt_r
    # # Tt1_r= state1.Tt_r
    # # Tt2_r = state2.Tt_r
    # Pt2 = P2 + 0.5*state2.rho*state2.V**2
    # # eta_r_ts = ((P2/Pt1_r)**((gamma-1)/gamma)-1)  /  (Tt2/Tt1-1)
    # # eta_r_tt = ((Pt2_r/Pt1_r)**((gamma-1)/gamma)-1)  /  (Tt2/Tt1-1)
    # eta_r_tt = ((Pt2/Pt1)**((gamma-1)/gamma)-1)  /  (Tt2/Tt1-1)
    # Tt2_is = Tt1*(Pt2/Pt1)**((gamma-1)/gamma)
    # eta_book= (Tt2-Tt2_is)/(Tt1-T1)
    # eta_basic1 = (Tt2_is-Tt1)/(Tt2-Tt1)
    # print("eta_r_tt = ",eta_r_tt)
    # # state2.Pt_r = Pt2_r
    # # state2.P = P2

    
    # #---------------------
    # Pt2 = P2 + 0.5*state2.rho*state2.V**2
    # # T2_istest =state1.Tt*(Pt2/state1.Pt)**((gamma-1)/gamma)
    # Tt2_is = state1.Tt*(Pt2/state1.Pt)**((gamma-1)/gamma)
    # T2_is = state1.Tt*(state2.P/state1.Pt)**((gamma-1)/gamma)

    # L_eul_t = settings.Cp*(state2.Tt-state1.Tt)
    # work = state1.U*(state2.Vt-state1.Vt)
    # eta_basic2 = (Tt2_is-Tt1)/(Tt2-Tt1)
    # eta_TS_m = settings.Cp*(T2_is-state1.Tt) / L_eul_t
    # eta_TT_m = settings.Cp*(Tt2_is-state1.Tt) / L_eul_t

    #---------------------

    return (eta_r_new, state2)


    

def stator_outlet(settings,state1,state2):

    # Inport variables -----------------------------------------------------------------------#
    eta_tt = settings.eta_TT_stage
    alpha3 = settings.alpha3
    R  = settings.R
    Cp = settings.Cp
    gamma = settings.gamma    
    #-----------------------------------------------------------------------------------------#

    state3 = State()
    # state3.vel_triangles(Va3_m,Vt3_m,U_m)
 
    U_m = 0
    Va3_m = state2.Va                                   # It's just an initial guess
    Vt3_m = Va3_m * math.tan(math.radians(alpha3))
    err = 1 
    j=0
    while err > 1e-5:                  # 9) Iterate over Va2_m i order to mantain continuity eq. for fixed b
        state3.vel_triangles(Va3_m,Vt3_m,U_m)

        # 8) Find thermodynamic state in section 2
        state3.thermodynamic_3(settings,state1,state2,eta_tt,R,Cp,gamma)
        # state3.B_L(settings)
        Va3_m_new = (state1.rho * state1.Va)/state3.rho
        err = abs(Va3_m_new-Va3_m)
        
        j+=1
        Va3_m=Va3_m_new
    
    return state3





def stator_losses(settings,state3,state2):
    # 13) Insert losses
    state3_losses=Losses()
    #13.1) Endwall losses (annulus + secondary) - Howell model
    state3_losses.endwall_losses(settings,state2,state3)
    #13.2) Tip clearance losses - Lakshiminarayana
    state3_losses.tip_losses(settings,state2,state3)
    #13.3) Profile losses - Lieblein
    state3_losses.profile_losses('stator',settings,state2,state3)

    return state3_losses

    


def eta_tt_losses(settings,state1,state2,state3,state3_losses):

    # Inport variables -----------------------------------------------------------------------#
    gamma = settings.gamma    
    #-----------------------------------------------------------------------------------------#

    Y = state3_losses.Y
    delta_eta = state3_losses.delta_eta
    Pt2 = state2.Pt
    T2 = state2.T
    T3 = state3.T
    P2 = state2.P

    P1=state1.P
    Pt1=state1.Pt
    T1=state1.T
    Tt1=state1.Tt
    Pt3_is = state2.Pt2_is
    Tt3 = state3.Tt

    Pt3 = Pt2 - (Pt2 - P2)*Y
    

    P3 = Pt3 - 0.5*state3.rho*state3.W**2              # From Pt2_r i can find P2. Use it to find eta_r (see Baksharone pg. 235)
    # eta_tt_new = ((P3/P2)**((gamma-1)/gamma)-1)/(T3/T2 -1)
    eta_tt_new = ((Pt3_is/Pt1)**((gamma-1)/gamma)-1)/(Tt3/Tt1 -1)
    # eta_tt_new = ((Pt3/Pt1)**((gamma-1)/gamma)-1)/(Tt3/Tt1 -1)
    # Tt_out_s = Tt1 * (Pt3_is/Pt1) ** ((gamma - 1)/gamma)
    # eta_tt_new = (Tt_out_s - Tt1) / (Tt3 - Tt1)
    


    eta_tt_new = eta_tt_new - delta_eta
    state3.Pt = Pt3
    state3.P = P3

    return (eta_tt_new, state3)