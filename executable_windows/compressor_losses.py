import numpy as np
import math


class Losses:

    def __init__(self):
        self.Y = 0
        self.delta_eta = 0
        self.fblock = 0                     # To be precise we should differentiate between LE fblock for the first row and TE fblock for the last one.
                                            # Unfortunately this model does not distinguish the location of the doundary layer displacement (mean)

    #13.1) Endwall losses (annulus + secondary) - Howell model
    def endwall_losses(self,settings,state1,state2):

        # Inport variables -----------------------------------------------------------------------#
        sigma = settings.sigma
        s = settings.s
        b = settings.b
        #-----------------------------------------------------------------------------------------#
 

        beta1 = math.radians(state1.beta)
        beta2 = math.radians(state2.beta)
        Cd_a = 0.02 * s/b
        beta_m = np.arctan(0.5*(math.tan(beta1)+math.tan(beta2)))
        Cl = 2/sigma * (math.tan(beta1)-math.tan(beta2))*math.cos(beta_m)
        Cd_s = 0.018 * Cl**2
        Y_endwall = ((Cd_a + Cd_s)*sigma*(math.cos(beta1))**2) / (math.cos(beta_m))**3
        self.Y += Y_endwall

    #13.2) Tip clearance losses - Lakshiminarayana
    def tip_losses(self,settings,state1,state2):

        # Inport variables -----------------------------------------------------------------------#
        tip_cl = settings.tip_cl
        b = settings.b
        c = settings.c
        psi = settings.psi
        phi = settings.phi
        #-----------------------------------------------------------------------------------------#

        alpha_av = 0.5*math.radians(state1.alpha+state2.alpha)
        delta_eta_tip = 0.07*tip_cl*psi/(b*math.cos(alpha_av)) * ( 1+10*np.sqrt(phi*tip_cl/c/psi/math.cos(alpha_av)) )
        self.delta_eta = delta_eta_tip

    #13.3) Profile losses - Lieblein
    def profile_losses(self,row,settings,state1,state2):

        # Inport variables -----------------------------------------------------------------------#
        sigma = settings.sigma
        H = settings.H
        c = settings.c
        b = settings.b
        #-----------------------------------------------------------------------------------------#

        if row=='rotor':
             W1 = state1.W
             W1t = state1.Wt
             W2 = state2.W
             W2t = state2.Wt
             beta1 = math.radians(state1.beta)
             beta2 = math.radians(state2.beta)
             D = (W1-W2)/W1 + abs( (W1t-W2t)/2/sigma/W1 )
             theta_c = 0.2940*D**3 - 0.2001*D**2 + 0.0476*D + 0.0028                                          # Least square fitting of the Lieblein loading criterion graph
             Y_profile = 2*theta_c*sigma/math.cos(beta2)*(math.cos(beta1)/math.cos(beta2))**2 * (2*H/(3*H-1)) / ( 1-theta_c*sigma*H/math.cos(beta2) )**3
             self.fblock = H * theta_c * c / b
       
        elif row=='stator':
             V1 = state1.V
             V1t = state1.Vt
             V2 = state2.V
             V2t = state2.Vt
             alpha1 = math.radians(state1.alpha)
             alpha2 = math.radians(state2.alpha)
             D = (V1-V2)/V1 + abs( (V1t-V2t)/2/sigma/V1 )
             theta_c = 0.2940*D**3 - 0.2001*D**2 + 0.0476*D + 0.0028                                          # Least square fitting of the Lieblein loading criterion graph
             Y_profile = 2*theta_c*sigma/math.cos(alpha2)*(math.cos(alpha1)/math.cos(alpha2))**2 * (2*H/(3*H-1)) / ( 1-theta_c*sigma*H/math.cos(alpha2) )**3
             self.fblock = H * theta_c * c / b

        self.Y += Y_profile

