

# Script containing all the equations needed to find the parameters used for blade shape modeling
# using lieblein criteria


def lieblein_relations(shape,sigma,t_max,beta1,theta):

    
    
    i0 = get_i0(shape,sigma,t_max,beta1)
    n = get_n(sigma,beta1)
    d0 = get_d0(shape,sigma,t_max,beta1)
    m = get_m(shape,beta1)
    b = get_b(beta1)


    i = i0 + n*theta
    delta = d0 + m*theta/(sigma**b)

    return (i, delta)
   
    # Define the lieblein relations to find incidence i0. I must find:
        # - Incidence shape factor               Ki_sh
        # - Incidence thickness factor           Ki_t
        # - Reference incidence at t_max = 10%   i0_10
        # - Coefficient                          n
        
   
def get_i0(shape,sigma,t_max,beta1):

    Ki_sh = get_Ki_sh(shape)
    Ki_t = get_Ki_t(t_max)
    i0_10 = get_i0_10(sigma,beta1)

    i0 = Ki_sh * Ki_t * i0_10
    return i0
       

def get_Ki_sh (shape):
    if shape == "Naca65":
         Ki_sh = 1
    elif shape == "double circular arc":
         Ki_sh = 0.70
    elif shape == "circular arc":
         Ki_sh = 1.1
    return Ki_sh

def get_Ki_t (t_max):
        
    Ki_t=230.4416*t_max**3 -103.1548*t_max**2 +17.9976*t_max +0.0017         # 3rd order polynomial interp
         
    return Ki_t
    
def get_i0_10(sigma,beta1):

    if sigma == 1:
        i0_10 = 1.9126e-6*beta1**3 -5.1576e-4*beta1**2 +0.1001*beta1 +0.0170 # 3rd order polynomial interp
    elif sigma == 2:
        i0_10 = -7.7527e-7*beta1**3+5.1994e-4*beta1**2 +0.1523*beta1 +0.0210 # 3rd order polynomial interp

    return i0_10

def get_n(sigma,beta1):

    if sigma == 1:
        # insert polynomial fitting
        n = -9.9337e-7*beta1**3 +7.0354e-5*beta1**2 -0.0049*beta1 +0.0089
    elif sigma == 2:
        # insert polynomial fitting
        n = -5.1709e-7*beta1**3 +1.4997e-6*beta1**2 +5.9620e-5*beta1 -0.0155
        
    return n





    
# Define the lieblein relations to find deviation d0. I must find:
    # - Deviation shape factor               Kd_sh
    # - Deviation thickness factor           Kd_t
    # - Reference deviation at t_max = 10%   d0_10
    # - Coefficient                          m
   
   
def get_d0(shape,sigma,t_max,beta1):

    Kd_sh = get_Kd_sh(shape)
    Kd_t = get_Kd_t(t_max)
    d0_10 = get_d0_10(sigma,beta1)

    d0 = Kd_sh * Kd_t * d0_10
        
    return d0


def get_Kd_sh (shape):
    if shape == "Naca65":
        Kd_sh = 1
    elif shape == "double circular arc":
        Kd_sh = 0.75
    elif shape == "circular arc":
        Kd_sh = 1.1
    return Kd_sh

def get_Kd_t (t_max):
        
    Kd_t=248.1332*t_max**3 -9.9524*t_max**2 +8.852*t_max -0.0206       # 3rd order polynomial interp
         
    return Kd_t
    
def get_d0_10(sigma,beta1):

    if sigma == 1:
        d0_10 = 1.3323e-5*beta1**3 -0.0011*beta1**2 +0.0517*beta1 -0.3090 # 3rd order polynomial interp
    elif sigma == 2:
        d0_10 = 2.0289e-5*beta1**3 -0.0010*beta1**2 +0.0399*beta1 -0.0062 # 3rd order polynomial interp

    return d0_10

def get_m(shape,beta1):

    if shape == "Naca65":
        # insert polynomial fitting
        m = 4.9278e-7*beta1**3 -1.9793e-5*beta1**2 +0.0013*beta1 +0.1582
    elif shape == "double circular arc" or shape == "circular arc":
        m = 1.6471e-8*beta1**3 +1.5608e-5*beta1**2 +1.2318e-4*beta1 +0.2502
        
    return m
    
def get_b(beta1):

    b = -2.4885e-6*beta1**3 +1.3296e-4*beta1**2 -0.0039*beta1 +0.9645

    return b




