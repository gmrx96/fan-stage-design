%interpolazione grafico lieblein

clear all 
close all
clc

%  %% ------------- Regressione grafico (D,theta/c)------------ %%
% x=[0.002 0.05 0.1 0.2 0.3 0.4 0.6 0.65 0.67];
% y=[0.004 0.004 0.005 0.006 0.008 0.01 0.02 0.03 0.035];
% 
% xq= 0:0.01:1;
% p = polyfit(x,y,3);
% x1 = linspace(0,1);
% y1 = polyval(p,x1);
% 
% y2= 0.2940*x1.^3-0.2001*x1.^2+0.0476*x1+0.0028;
% 
% figure()
% plot (x,y,'x')
% hold on
% plot(x1,y1)
% hold on
% plot(x1,y2,'--b')
% axis([0 1 0 0.1])
% 

% %%  ------------- Regressione grafico (beta,i0_10)------------ %%
% ---- sigma =1-------
% x=[0 20 40 60 70];
% y=[0 1.9 3.2 4.7 5.1];
%---------------------

% ---- sigma =2-------
% x=[0 20 40 60 70];
% y=[0 3.3 6.3 9.5 10.5];
%---------------------
% xq= 0:1:90;
% p = polyfit(x,y,3);
% x1 = linspace(0,90);
% y1 = polyval(p,x1);
% 
% figure()
% plot (x,y,'x')
% hold on
% plot(x1,y1)
% axis([0 90 0 10])

% %%  ------------- Regressione grafico (beta,n)------------ %%
% 

% ---- sigma =1-------
% x=[20 30 40 52 60 66 70];
% y=[-0.07 -0.1 -0.14 -0.2 -0.24 -0.3 -0.33];
% -----------------

% ---- sigma =2-------
% x=[0 20 40 56 60 70];
% y=[-0.015 -0.02 -0.04 -0.1 -0.12 -0.18];
%----------------------

% xq= 0:1:90;
% p = polyfit(x,y,3);
% x1 = linspace(0,90);
% y1 = polyval(p,x1);
% 
% figure()
% plot (x,y,'x')
% hold on
% plot(x1,y1)
% % axis([0 90 0 10])

% %%  ------------- Regressione grafico (t_max,Ki_t)------------ %%
% 
% x=[0.012 0.02 0.026 0.04 0.06 0.08 0.1];
% y=[0.2 0.33 0.4 0.57 0.76 0.9 1];

% xq= 0:1:90;
% p = polyfit(x,y,3);
% x1 = linspace(0,90);
% y1 = polyval(p,x1);
% 
% figure()
% plot (x,y,'x')
% hold on
% plot(x1,y1)
% axis([0 90 0 10])

% %%  ------------- Regressione grafico (beta,delta0.10)------------ %%
% 
% ---- sigma =1-------
% x=[20 40 44 60 63 70];
% y=[0.4 0.9 1 1.8 2 2.6];
% -------------------
%
% ---- sigma =2-------
x=[0 20 34 40 52 60 65 70];
y=[0 0.5 1 1.3 2 3 4 4.6];
%------------------
xq= 0:1:90;
p = polyfit(x,y,3);
x1 = linspace(0,90);
y1 = polyval(p,x1);

figure()
plot (x,y,'x')
hold on
plot(x1,y1)
% axis([0 90 0 10])

% %%  ------------- Regressione grafico (t_max,Kdelta_t)------------ %%
% 
% x=[0.025 0.04 0.047 0.067 0.08 0.083 0.1 0.112];
% y=[0.2 0.33 0.4 0.6 0.75 0.8 1 1.2];
% 
% xq= 0:1:2;
% p = polyfit(x,y,3);
% x1 = linspace(0,2);
% y1 = polyval(p,x1);
% 
% figure()
% plot (x,y,'x')
% hold on
% plot(x1,y1)
% axis([0 2 0 2])

% %%  ------------- Regressione grafico (beta1,m)NACA65------------ %%
% 
% x=[20 35 40 60 66 70];
% y=[0.18 0.2 0.21 0.27 0.3 0.32];
% 
% xq= 0:1:90;
% p = polyfit(x,y,3);
% x1 = linspace(0,90);
% y1 = polyval(p,x1);
% 
% figure()
% plot (x,y,'x')
% hold on
% plot(x1,y1)
% axis([0 90 0 2])

% %%  ------------- Regressione grafico (beta1,m)circular arc------------ %%
% 
% x=[0 20 40 52 60 70];
% y=[0.25 0.26 0.28 0.3 0.32 0.34];
% 
% xq= 0:1:90;
% p = polyfit(x,y,3);
% x1 = linspace(0,90);
% y1 = polyval(p,x1);
% 
% figure()
% plot (x,y,'x')
% hold on
% plot(x1,y1)
% axis([0 90 0 2])

%%  ------------- Regressione grafico (beta1,b)------------ %%
% 
% x=[0 10 20 30 40 48 58 60 67 70];
% y=[0.96 0.94 0.93 0.9 0.85 0.8 0.7 0.68 0.6 0.45];
% 
% xq= 0:1:90;
% p = polyfit(x,y,3);
% x1 = linspace(0,90);
% y1 = polyval(p,x1);
% 
% figure()
% plot (x,y,'x')
% hold on
% plot(x1,y1)
% axis([0 90 0 2])